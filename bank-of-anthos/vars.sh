#!/usr/bin/env bash
export KCC_GKE=kcc
export KCC_GKE_ZONE=us-central1-f

export HOST_NET_PROJECT=proj-0-net-prod
export SVC_1_PROJECT=proj-1-ops-prod
export SVC_2_PROJECT=proj-2-bank-prod
export SVC_3_PROJECT=proj-3-shop-prod

# Shared VPC
export SHARED_VPC=shared-vpc

export SUBNET_1_REGION=us-west2
export SUBNET_1_NAME=subnet-1-${SUBNET_1_REGION}
export SUBNET_1_RANGE=10.4.0.0/22
export SUBNET_1_POD_NAME=${SUBNET_1_NAME}-pods
export SUBNET_1_POD_RANGE=10.0.0.0/14
export SUBNET_1_SVC_1_NAME=${SUBNET_1_NAME}-svc-1
export SUBNET_1_SVC_1_RANGE=10.5.0.0/20
export SUBNET_1_SVC_2_NAME=${SUBNET_1_NAME}-svc-2
export SUBNET_1_SVC_2_RANGE=10.5.16.0/20
export SUBNET_1_SVC_3_NAME=${SUBNET_1_NAME}-svc-3
export SUBNET_1_SVC_3_RANGE=10.5.32.0/20

export SUBNET_2_REGION=us-central1
export SUBNET_2_NAME=subnet-2-${SUBNET_2_REGION}
export SUBNET_2_RANGE=10.12.0.0/22
export SUBNET_2_POD_NAME=${SUBNET_2_NAME}-pods
export SUBNET_2_POD_RANGE=10.8.0.0/14
export SUBNET_2_SVC_1_NAME=${SUBNET_2_NAME}-svc-1
export SUBNET_2_SVC_1_RANGE=10.13.0.0/20
export SUBNET_2_SVC_2_NAME=${SUBNET_2_NAME}-svc-2
export SUBNET_2_SVC_2_RANGE=10.13.16.0/20
export SUBNET_2_SVC_3_NAME=${SUBNET_2_NAME}-svc-3
export SUBNET_2_SVC_3_RANGE=10.13.32.0/20

export SUBNET_3_REGION=us-west2
export SUBNET_3_NAME=subnet-3-${SUBNET_3_REGION}
export SUBNET_3_RANGE=10.20.0.0/22
export SUBNET_3_POD_NAME=${SUBNET_1_NAME}-pods
export SUBNET_3_POD_RANGE=10.16.0.0/14
export SUBNET_3_SVC_1_NAME=${SUBNET_1_NAME}-svc-1
export SUBNET_3_SVC_1_RANGE=10.21.0.0/20
export SUBNET_3_SVC_2_NAME=${SUBNET_1_NAME}-svc-2
export SUBNET_3_SVC_2_RANGE=10.21.16.0/20
export SUBNET_3_SVC_3_NAME=${SUBNET_1_NAME}-svc-3
export SUBNET_3_SVC_3_RANGE=10.21.32.0/20

export SUBNET_4_REGION=us-central1
export SUBNET_4_NAME=subnet-4-${SUBNET_4_REGION}
export SUBNET_4_RANGE=10.28.0.0/22
export SUBNET_4_POD_NAME=${SUBNET_1_NAME}-pods
export SUBNET_4_POD_RANGE=10.24.0.0/14
export SUBNET_4_SVC_1_NAME=${SUBNET_1_NAME}-svc-1
export SUBNET_4_SVC_1_RANGE=10.29.0.0/20
export SUBNET_4_SVC_2_NAME=${SUBNET_1_NAME}-svc-2
export SUBNET_4_SVC_2_RANGE=10.29.16.0/20
export SUBNET_4_SVC_3_NAME=${SUBNET_1_NAME}-svc-3
export SUBNET_4_SVC_3_RANGE=10.29.32.0/20

export ASM_MAJOR_VERSION=1.8
export ASM_VERSION=1.8.2-asm.2
export ASM_LABEL=asm-182-2
export CLOUDSHELL_IP=$(curl ifconfig.me)

# GKE clusters
export GKE1=gke-1-r1a-prod
export GKE2=gke-2-r1b-prod
export GKE3=gke-3-r2a-prod
export GKE4=gke-4-r2b-prod
export GKE1_ZONE=${SUBNET_1_REGION}-a
export GKE2_ZONE=${SUBNET_1_REGION}-b
export GKE3_ZONE=${SUBNET_2_REGION}-a
export GKE4_ZONE=${SUBNET_2_REGION}-b
export GKE1_MASTER_IPV4_CIDR=172.16.0.0/28
export GKE2_MASTER_IPV4_CIDR=172.16.1.0/28
export GKE3_MASTER_IPV4_CIDR=172.16.2.0/28
export GKE4_MASTER_IPV4_CIDR=172.16.3.0/28
export GKE5_INGRESS_MASTER_IPV4_CIDR=172.16.4.0/28
export GKE5_INGRESS=ingress-config
export GKE5_INGRESS_ZONE=us-west2-a

export BANK_OF_ANTHOS_DB="bank-of-anthos-db"
export BANK_OF_ANTHOS_DB_USER="admin"
export BANK_OF_ANTHOS_DB_PASSWORD="admin"
export BANK_OF_ANTHOS_LEDGER_DB="ledger-db"
export BANK_OF_ANTHOS_ACCOUNTS_DB="accounts-db"
export BANK_OF_ANTHOS_GSA="bank-of-anthos-gsa"
export BANK_OF_ANTHOS_NAMESPACE="bank-of-anthos"
export BANK_OF_ANTHOS_KSA="bank-of-anthos-ksa"
export BANK_OF_ANTHOS_INGRESS_IP_NAME="bank-of-anthos-ingress-ip"
export BANK_OF_ANTHOS_INGRESS_CERT="bank-of-anthos-ingress-cert"