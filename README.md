# Setting up ASM service mesh in a single service projects on a shared VPC and private clusters using a Gitlab pipeline with Kubernetes Config Connector

&nbsp;
<img src="/admin/docs/arch.svg" width=100% height=100%>
&nbsp;
&nbsp;
<img src="/admin/docs/infra-complete-pipeline.png" width=100% height=100%>
&nbsp;

[[_TOC_]]

_Last tested 11/19/2021 by ameer00@_

This tutorial shows how to run distributed services on multiple GKE clusters in multiple service projects on a shared VPC in GCP using Anthos Service Mesh (ASM). A distributed service is a Kubernetes Service that runs on multiple Kubernetes clusters in the same namespace and acts as a single logical service. Distributed services are more resilient than Kubernetes Services because they run on multiple GKE clusters. A distributed service remains up even if one or more GKE clusters are down, as long as the healthy clusters are able to serve the desired load.  
Kubernetes Services are known only to the Kubernetes API server of the cluster they run on. If the Kubernetes cluster is down (for example during a scheduled maintenance), so are all of the Kubernetes Services running on that cluster. Running distributed services makes cluster lifecycle management easier since clusters can be brought down for maintenance or upgrades while other cluster service service traffic. In order to create a distributed service, service mesh functionality provided by ASM is used to "glue" services running on multiple clusters together to act as a single logical service.

This guide shows how to set up a shared VPC with a single service project owned by the platform administrator team. Then, how to create multiple GKE private clusters in multiple service projects, how to deploy ASM on multiple GKE private clusters and how to run distributed services on this platform. You may use the same guide for non-private GKE clusters as well. The configuration intended strictly for private clusters is highlighted in this guide.

This guide is intended for platform administrators and service operators with basic knowledge of Kubernetes. Some knowledge of service mesh is beneficial although not necessary. ASM is based on the OSS Istio technology. You can learn more about service mesh and Istio on [istio.io](istio.io).

## Kubernetes Config Connector

This guide primarily uses [Kubernetes Config Connector](https://cloud.google.com/config-connector/docs/overview) for infrastructure resource management in GCP. Config Connector is a Kubernetes add-on that allows you to manage Google Cloud resources through Kubernetes. You use declarative Kubernetes manifests to deploy GCP resources for example projects, networks and GKE clusters. The configurations in this guide can be used in an infrastructure deployment pipeline. All configs can be stored in a Git repo and can follow the typical Gitops methodology for infrastructure deployment.  
In this guide, you create a folder for KCC and a project called `proj-infra-admin` inside that folder. This project contains a single GKE cluster called `kcc` with the Config Connector addon. The `proj-infra-admin` project and `kcc` cluster belong to the cloud infrastructure admin team who are responsible for configuring resources in GCP. You create a GCP Service Account with the required permissions and using [workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity) associate it with a Kubernetes service account used by the Config Connector to provision GCP resources. No other resources are provisioned in this project. The `proj-infra-admin` project and the `kcc` cluster are used solely for cloud resource deployment.

## Organization and Architecture

For the purpose of this guide, imagine your organization is divided into different teams based on functions. There are four teams in your organization:

- **Networking team** - a centralized team of cloud networking experts responsible for managing GCP networks. They own, manage and maintain the VPC and other networking resources for the entire organization.
- **Platform team** - a centralized platform administrator team. This team is responsible for maintaining and managing the platform where application teams run their applications. This team is responsible for GKE and service mesh administration. They own, manage and maintain the platform inside a GCP project.
- **Product bank team** - an application team responsible for creating the `bank` application or product. This team manages and maintains resources to run the `bank` application. The application runs on GKE clusters in the polatform administrators project. Each product team gets a `landing zone` on the GKE platform where containerized applications run. The landing zone is composed of separate namespaces per service and a set of policies and configurations required for that service. All non GKE resources run inside the `bank` GCP project.
- **Product shop team** - an application team responsible for creating the `shop` application or product. This team manages and maintains resources to run the `shop` application. The application runs on GKE clusters in the platform administrators project. Similar to the `bank` product, the `shop` product gets a landing zone on the GKE platform to run their containerized applications. All non GKE resources run inside the `shop` GCP project.

In this guide, you are responsible for building a cloud architecture to run the two products - `bank` and `shop`.

## Objective

- Create four projects. One for the shared VPC (also the network host project), one for the `platform_admins` team, and two for the `bank` and `shop` product.
- Create and configure a shared VPC, create subnets and share subnets with the three service projects.
- Create four GKE private clusters in the `platform_admins` project.
- Configure networking (NAT Gateways, Cloud Router and firewall rules) to allow inter-cluster and egress traffic from the four private GKE clusters. Also allow API service access from your terminal (Cloud Shell) to the four private GKE clusters using authorized networks.
- Deploy and configure multi cluster ASM to the four GKE clusters in multi-primary mode. Multi-primary mode deploys ASM controlplane in all clusters.
- Deploy the Bank of Anthos application on all four clusters in the `platform_admins` project in the `bank-of-anthos` namespace. All services except the databases are deployed as distributed services (Pods running on all clusters). The Cloud SQL database runs in the `bank` service project owned by the `bank` product team.
- Deploy the Online Boutique application on all four clusters in the `platform_admins` project in the `online-boutique` namespace. All services except the Redis database are deployed as distributed services (Pods running on both clusters). RedisDB is deployed in the `shop` project owned by the `shop` product team.
- Access the two applications via ASM Ingress.

## Costs

This tutorial uses the following billable components of Google Cloud Platform:

- [GKE](https://cloud.google.com/kubernetes-engine/pricing)
- [Networking](https://cloud.google.com/vpc/network-pricing#vpc-pricing)
- [Load Balancing](https://cloud.google.com/vpc/network-pricing#lb)

Use the [Pricing Calculator](https://cloud.google.com/products/calculator) to generate a cost estimate based on your projected usage.

## Before you begin

> This guide may not work on a Mac terminal. It is recommended (and has been tested) on Cloud Shell.

1.  In the Cloud Console, activate Cloud Shell.

    [ACTIVATE CLOUD SHELL](https://console.cloud.google.com/?cloudshell=true)

At the bottom of the [Cloud Console](https://cloud.google.com/shell/docs/features), a Cloud Shell session starts and displays a command-line prompt. Cloud Shell is a shell environment with the Cloud SDK already installed, including the [gcloud](https://cloud.google.com/sdk/gcloud) command-line tool, and with values already set for your current project. It can take a few seconds for the session to initialize.  
This guide requires a GCP organization because of shared VPC. Your user must also be an Organization Admin and a Billing Account Admin so you can create resources in the org and assign billing accounts to the new projects.  
This guide also requires a [gitlab.com](gitlab.com) account. You will create the infrastructure pipeline on gitlab.com.

## Setting up the environment

1.  You can create multiple environments by setting a two digit build number.

    ```bash
    export BUILD_NUMBER=01 # Enter a different build number for multiple environments
    ```

1.  Create a `WORKDIR` for this guide. At the end you can delete all the resources and this folder.

    ```bash
    mkdir -p asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER} && cd asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER} && export WORKDIR=`pwd`
    ```

1.  Create environment variables used in this guide. You use environment variables throughout this guide. It is best to save them in a `vars.sh` file, in case you lose access to your terminal (and your envars), you can source the vars.sh.

    ```bash
    export ADMIN_USER=[Enter Org admin email for example you@org.xyz]
    export ORG_NAME=[Your Organization name for example org.xyz]
    export BILLING_ACCOUNT=[Enter Billing Account ID for example C0FFEE-123456-AABBCC]
    export ORG_ID=[Enter Org ID for example 1234567890 from gcloud organizations list]
    ```

1.  Copy the remainder of the variables in your `vars.sh` file.

    ```bash
    export ORG_SHORT_NAME=${ORG_NAME%.*}
    export USER_NAME=${ADMIN_USER%@*}
    export USER_NAME=${USER_NAME:0:7}
    export SUBNET_1_REGION=us-west2
    export SUBNET_2_REGION=us-central1
    export BUILD_NUMBER=${BUILD_NUMBER}
    cat <<EOF >> $WORKDIR/vars.sh
    # Copy and paste the remainder of the variables
    # Org
    export BRANCH_NAME=main
    export WORKDIR=$WORKDIR
    export ADMIN_USER=$ADMIN_USER
    export ORG_NAME=$ORG_NAME
    export BILLING_ACCOUNT=$BILLING_ACCOUNT
    export ORG_ID=$ORG_ID
    export ORG_SHORT_NAME=${ORG_SHORT_NAME}
    export USER_NAME=${USER_NAME}
    export BUILD_NUMBER=${BUILD_NUMBER}
    export MAIN_FOLDER_NAME=${USER_NAME}-${BUILD_NUMBER}-asm-shared-single
    # KCC
    export KCC_FOLDER_NAME=kcc
    export KCC_PROJECT=proj-infra-admin
    export KCC_GKE=kcc
    export KCC_GKE_LOCATION=us-central1
    export KCC_SERVICE_ACCOUNT=kcc-sa
    # GCP Infra
    export FOLDER_NAME=infra
    export HOST_NET_PROJECT=proj-0-net-prod
    export SVC_1_PROJECT=proj-1-ops-prod
    export SVC_2_PROJECT=proj-2-bank-prod
    export SVC_3_PROJECT=proj-3-shop-prod
    # Gitlab groups and projects
    export GITLAB_CI_GCP_SA_NAME=gitlab-ci-sa
    export GITLAB_GROUP_NAME=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm
    export GITLAB_ADMIN_SUBGROUP_NAME=platform-admins-group
    export GITLAB_BANK_SUBGROUP_NAME=bank-of-anthos-group
    export GITLAB_SHOP_SUBGROUP_NAME=online-boutique-group
    export GITLAB_DATABASES_SUBGROUP_NAME=databases-group
    export GITLAB_INFRA_PROJECT_NAME=infrastructure
    export GITLAB_CONFIG_PROJECT_NAME=config
    export GITLAB_CD_PROJECT_NAME=shared-cd
    ## Bank of Anthos repos
    export GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME=accounts-db
    export GITLAB_BANK_LEDGER_DB_PROJECT_NAME=ledger-db
    export GITLAB_CONTACTS_PROJECT_NAME=contacts
    export GITLAB_BANKFRONTEND_PROJECT_NAME=bankfrontend
    export GITLAB_BALANCEREADER_PROJECT_NAME=balancereader
    export GITLAB_LEDGERWRITER_PROJECT_NAME=ledgerwriter
    export GITLAB_BANKLOADGENERATOR_PROJECT_NAME=bankloadgenerator
    export GITLAB_TRANSACTIONHISTORY_PROJECT_NAME=transactionhistory
    export GITLAB_USERSERVICE_PROJECT_NAME=userservice
    ## Online Boutique repos
    export GITLAB_AD_PROJECT_NAME=ad
    export GITLAB_CART_PROJECT_NAME=cart
    export GITLAB_CHECKOUT_PROJECT_NAME=checkout
    export GITLAB_CURRENCY_PROJECT_NAME=currency
    export GITLAB_EMAIL_PROJECT_NAME=email
    export GITLAB_SHOPFRONTEND_PROJECT_NAME=shopfrontend
    export GITLAB_SHOPLOADGENERATOR_PROJECT_NAME=shoploadgenerator
    export GITLAB_PAYMENT_PROJECT_NAME=payment
    export GITLAB_PRODUCTCATALOG_PROJECT_NAME=productcatalog
    export GITLAB_RECOMMENDATION_PROJECT_NAME=recommendation
    export GITLAB_SHIPPING_PROJECT_NAME=shipping
    export GITLAB_SHOP_REDIS_PROJECT_NAME=shopredis
    export GITLAB_TEST_PROJECT_NAME=test
    ## Single repos
    export GITLAB_BANK_PROJECT_NAME=bank-of-anthos
    export GITLAB_SHOP_PROJECT_NAME=online-boutique
    export GITLAB_COCKROACHDB_PROJECT_NAME=cockroachdb
    export GITLAB_REDIS_PROJECT_NAME=redis
    export GITLAB_SSH_KEY=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm-shared-single-sshkey
    # GKE clusters and zones
    export GKE1=gke-1-r1a-prod
    export GKE2=gke-2-r1b-prod
    export GKE3=gke-3-r2a-prod
    export GKE4=gke-4-r2b-prod
    export GKE5_INGRESS=ingress-config
    export GKE1_ZONE=${SUBNET_1_REGION}-a
    export GKE2_ZONE=${SUBNET_1_REGION}-b
    export GKE3_ZONE=${SUBNET_2_REGION}-a
    export GKE4_ZONE=${SUBNET_2_REGION}-b
    export GKE5_INGRESS_ZONE=${SUBNET_1_REGION}-a
    export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    # ASM
    export ASM_VERSION=1.10.4-asm.6
    export ASM_LABEL=asm-managed
    export ASM_CHANNEL=regular
    # ACM
    export ACM_VERSION=1.6.1
    # My IP
    export TERMINAL_IP=$(curl -s ifconfig.me)
    EOF
    ```

1.  Source vars.sh. In case you lose your terminal, you must always source vars.sh before proceeding.

    ```bash
    source $WORKDIR/vars.sh
    ```

1.  Install [krew](https://krew.sigs.k8s.io). Krew is a kubectl plugin manager that helps you discover and manage kubectl plugins. Two of these plugins are ctx and ns which make switching between multiple GKE clusters easier.

    ```bash
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )

    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc # .zshrc if using ZSH
    ```

1.  Install `ctx` and `ns` via krew for easy context switching.

    ```bash
    kubectl krew install ctx
    kubectl krew install ns
    kubectl krew install get-all
    ```

## Creating folder, project and GKE cluster for KCC

In this section, you create a folder and a project for KCC. This is a project owned by infrastructure administrators used to create GCP resources for example folders, projects, networks, IAM permissions and GKE clusters.

1.  Update `gcloud` and install alpha and beta components.

    ```bash
    gcloud components install alpha beta
    gcloud components update
    ```

In Cloud Shell, you may have to use `sudo apt-get install` to install gcloud components. Follow the instructions in Cloud Shell. This step may take a few minutes to complete.

1.  Login with your admin user account.

    ```bash
    gcloud config set account ${ADMIN_USER}
    gcloud auth login
    gcloud config unset project
    ```

1.  Create a main folder. Inside the main folder are two sub-folders. One sub-folder contains GCP builder project. The other sub-folder is where the infrastructure resources are created. Creating folders allows you to easily keep track of resources required for this guide in one place. At the end, you can delete all resources in the main folder.

    ```bash
    # Create the main folder
    gcloud alpha resource-manager folders create \
    --display-name=${MAIN_FOLDER_NAME} \
    --organization=${ORG_ID}

    # Get Main folder ID
    export MAIN_FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} --filter="displayName=${MAIN_FOLDER_NAME}" --format='value(name)')
    [ -z "$MAIN_FOLDER_ID" ] && echo "MAIN_FOLDER_ID is not exported" || echo "MAIN_FOLDER_ID is $MAIN_FOLDER_ID"
    echo -e "export MAIN_FOLDER_ID=$MAIN_FOLDER_ID" >> $WORKDIR/vars.sh
    source ${WORKDIR}/vars.sh

    # Create the KCC builder folder inside the main folder
    gcloud alpha resource-manager folders create \
    --display-name=${KCC_FOLDER_NAME} \
    --folder=${MAIN_FOLDER_ID}

    # Get KCC folder ID
    export KCC_FOLDER_ID=$(gcloud resource-manager folders list --folder=${MAIN_FOLDER_ID} --filter="displayName=${KCC_FOLDER_NAME}" --format='value(name)')
    [ -z "$KCC_FOLDER_ID" ] && echo "KCC_FOLDER_ID is not exported" || echo "KCC Folder ID is $KCC_FOLDER_ID"
    echo -e "export KCC_FOLDER_ID=$KCC_FOLDER_ID" >> $WORKDIR/vars.sh
    ```

1.  Create a project for KCC.

    ```bash
    gcloud projects create ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --folder ${KCC_FOLDER_ID}
    gcloud beta billing projects link ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --billing-account ${BILLING_ACCOUNT}

    gcloud services enable \
      --project ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      container.googleapis.com \
      cloudbilling.googleapis.com \
      cloudbuild.googleapis.com \
      sqladmin.googleapis.com \
      servicenetworking.googleapis.com \
      accesscontextmanager.googleapis.com \
      cloudresourcemanager.googleapis.com

    DTSTART=$(date  --date="yesterday" +"%m-%d-%yT08:00:00Z")
    DTEND=$(date  --date="yesterday" +"%m-%d-%yT12:00:00Z")
    gcloud container clusters create ${KCC_GKE} \
    --project=${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --region=${KCC_GKE_LOCATION} \
    --node-locations=${KCC_GKE_LOCATION}-a,${KCC_GKE_LOCATION}-b \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "3" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --release-channel None \
    --no-enable-autoupgrade \
    --addons ConfigConnector \
    --maintenance-window-start ${DTSTART} \
    --maintenance-window-end ${DTEND} \
    --maintenance-window-recurrence FREQ=DAILY \
    --workload-pool=${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog

    touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    echo -e "export KUBECONFIG=$WORKDIR/asm-kubeconfig" >> $WORKDIR/vars.sh
    gcloud container clusters get-credentials ${KCC_GKE} --region ${KCC_GKE_LOCATION} --project ${KCC_PROJECT}-${KCC_FOLDER_ID}

    kubectl ctx ${KCC_GKE}=gke_${KCC_PROJECT}-${KCC_FOLDER_ID}_${KCC_GKE_LOCATION}_${KCC_GKE}
    ```

## Creating an identity for KCC

In this section, you configure identity for KCC to create GCP resources. Config Connector creates and manages Google Cloud resources by authenticating with a Identity and Access Management (IAM) service account and using GKE's Workload Identity to bind IAM service accounts with Kubernetes service accounts.

1.  Create a service account and assign Organization Admin, Folder Admin, Project Creator, Billing Account Admin and Compute Admin roles to the account.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${KCC_SERVICE_ACCOUNT}

    project_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
        roles/accesscontextmanager.policyAdmin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
          --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
          --role="${role}"
    done
    ```

1.  Give the KCC SA billing user permission to the billing account ID.

    ```bash
    gcloud beta billing accounts add-iam-policy-binding ${BILLING_ACCOUNT} --member=serviceAccount:"${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --role="roles/billing.user"
    ```

1.  Create an IAM policy binding between the KCC GCP service account and the predefined Kubernetes service account that Config Connector runs.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts add-iam-policy-binding \
    ${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --member="serviceAccount:${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
    --role="roles/iam.workloadIdentityUser"
    ```

The output is similar to the following:

    Updated IAM policy for serviceAccount [kcc-sa@proj-infra-admin-389773461808.iam.gserviceaccount.com].
    bindings:
    - members:
      - serviceAccount:proj-infra-admin-389773461808.svc.id.goog[cnrm-system/cnrm-controller-manager]
      role: roles/iam.workloadIdentityUser
    etag: BwW6ev3b5kc=
    version: 1

1.  Create the config connector resource.

    ```bash
    cat <<EOF > ${KCC_GKE}-configconnector.yaml
    apiVersion: core.cnrm.cloud.google.com/v1beta1
    kind: ConfigConnector
    metadata:
      # the name is restricted to ensure that there is only one
      # ConfigConnector instance installed in your cluster
      name: configconnector.core.cnrm.cloud.google.com
    spec:
      mode: cluster
      googleServiceAccount: "${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com"
    EOF

    kubectl --context=${KCC_GKE} apply -f ${KCC_GKE}-configconnector.yaml
    ```

The output is similar to the following:

    configconnector.core.cnrm.cloud.google.com/configconnector.core.cnrm.cloud.google.com configured

## Creating an organization namespace

In this section, you create a namespace for organization. All organization level resources like folders or projects are created in the organization namespace.

1.  Create an org namespace.

    ```bash
    cat <<EOF > ${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
    apiVersion: v1
    kind: Namespace
    metadata:
      name: ${ORG_SHORT_NAME}
      annotations:
        cnrm.cloud.google.com/organization-id: "${ORG_ID}"
    EOF

    kubectl --context=${KCC_GKE} apply -f ${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
    ```

## Creating service account for Gitlab CI

In this section, you create a GCP Service Account in the `proj-infra-admin` project that is used by the Gitlab CI to deploy infrastructure.

1.  Create a GCP service for Gitlab CI. Assign the appropriate IAM roles that the Gitlab CI needs to deploy resources to GCP.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${GITLAB_CI_GCP_SA_NAME}

    project_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
        roles/accesscontextmanager.policyAdmin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
          --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
          --role="${role}"
    done
    ```

1.  Download the Gitlab GCP SA credentials and create a base64 version for Gitlab CI.

    ```bash
    gcloud iam service-accounts keys create ${GITLAB_CI_GCP_SA_NAME}-key.json \
      --iam-account ${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com
    cat ${GITLAB_CI_GCP_SA_NAME}-key.json | base64 > ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt
    ```

## Creating the builder image for Gitlab CI

In this section, you create a Docker image with all the tools required to deploy resources for this guide.

1.  Clone the ASM repo.

    ```bash
    git clone https://gitlab.com/asm7/asm-shared-vpc-single-project.git -b ${BRANCH_NAME} ${WORKDIR}/asm-shared-vpc-single-project-in-workdir
    cd ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/infrastructure/builder
    ```

1.  Create a `builder` Docker image which is used by Gitlab CI. This image contains all the tools required to deploy resources to GCP. These tools include gcloud, kubectl, and common bash utilities. Push the image to the GCR repo in the `proj-infra-admin` project.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds submit . --tag=gcr.io/${KCC_PROJECT}-${KCC_FOLDER_ID}/builder
    ```

The output is similar to the following:

    ...
    33e1791e-f1a4-4c50-98d0-ff63338d3a4b  2021-02-09T19:11:13+00:00  2M22S     gs://proj-infra-admin-1053315125805_cloudbuild/source/1612897872.028883-1ae85c6643044bc4b247da6dff4d4412.tgz  gcr.io/proj-infra-admin-1053315125805/builder (+1 more)  SUCCESS

1.  Make the `builder` Docker image public so that Gitlab CI can use the image.

    ```bash
    gsutil defacl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -r -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    cd ${WORKDIR}
    ```

## Creating Gitlab group and projects

In this section, you create a Gitlab group (and subgroups) and an `infra` project and add the `kcc` cluster to the project so that Gitlab CI can use runners in the `kcc` cluster.

### Preparing Gitlab credentials

1.  Create an access token to Gitlab so that you can use Gitlab APIs to create and configure projects. Access the following link. Name the token and check **api** under **Scopes**. Click **Create personal access token**.

    ```bash
    echo -n "https://gitlab.com/-/profile/personal_access_tokens"
    ```

<img src="admin/docs/gitlab-api-token.png" width=90% height=90%>

Copy the personal access token to a notepad or Google Keep app. It is important not to share this token with anyone since it provides complete read/write API access to your Gitlab account. You can revoke the token after you're done.

1.  Create a variable with your Gitlab personal access token.

    ```bash
    export GITLAB_TOKEN=<Gitlab personal access token>
    echo -e "export GITLAB_TOKEN=$GITLAB_TOKEN" >> $WORKDIR/vars.sh
    ```

### Creating Gitlab group and configure shared Gitlab runners

1.  Create an SSH keypair to use with Gitlab to clone and update repos.

    ```bash
    mkdir -p ${WORKDIR}/tmp && cd ${WORKDIR}/tmp
    ssh-keygen -t rsa -b 4096 \
    -C "" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key
    cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key | base64 > ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key-base64.txt
    ssh-keygen -t rsa -b 4096 \
    -C "${ADMIN_USER}" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    eval `ssh-agent`
    ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    echo -e "eval \`ssh-agent\` && ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key" >> $WORKDIR/vars.sh
    ```

1.  Add the SSH public key to your Gitlab account.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" --form "title=${GITLAB_SSH_KEY}" --form "key=$(cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key.pub)"
    ```

1.  Get your Gitlab user ID.

    ```bash
    export GITLAB_USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/user | jq -r '.id')
    [ -z "$GITLAB_USER_ID" ] && echo "GITLAB_USER_ID is not exported" || echo "GITLAB_USER_ID is $GITLAB_USER_ID"
    echo -e "export GITLAB_USER_ID=$GITLAB_USER_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_USER_ID is 12345679

1.  Create a new Gitlab group using the Gitlab API and get the group ID.

    ```bash
    curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --form "name=${GITLAB_GROUP_NAME}" --form "path=${GITLAB_GROUP_NAME}" \
    --form "file=@${WORKDIR}/asm-shared-vpc-single-project-in-workdir/admin/scripts/gitlab-parent-group.tar.gz" \
      "https://gitlab.com/api/v4/groups/import"

    # Get group ID
    export GITLAB_GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
    [ -z "$GITLAB_GROUP_ID" ] && echo "GITLAB_GROUP_ID is not exported" || echo "GITLAB_GROUP_ID is $GITLAB_GROUP_ID"
    echo -e "export GITLAB_GROUP_ID=$GITLAB_GROUP_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_GROUP_ID is 123456789

1.  Get runner's registration code from the Gitlab group. You can use this code to deploy your Gitlab runners in the `kcc` cluster.

    ```bash
    export GITLAB_RUNNER_TOKEN=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID" | jq -r '.runners_token')
    [ -z "$GITLAB_RUNNER_TOKEN" ] && echo "GITLAB_RUNNER_TOKEN is not exported" || echo "GITLAB_RUNNER_TOKEN is $GITLAB_RUNNER_TOKEN"
    echo -e "export GITLAB_RUNNER_TOKEN=$GITLAB_RUNNER_TOKEN" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_RUNNER_TOKEN is abCDef1223DCba

Use helm v3 to deploy the runner in the `kcc` cluster. Otherwise, [install helm](https://helm.sh/docs/intro/install/).

1.  Verify you have helm v3 installed in your terminal.

    ```bash
    helm version
    ```

The output is similar to the following:

    version.BuildInfo{Version:"v3.5.0"...}

1.  Deploy the helm chart.

    ```bash
    helm repo add gitlab https://charts.gitlab.io

    kubectl --context=${KCC_GKE} create namespace gitlab

    helm install --namespace gitlab gitlab-runner --set gitlabUrl="https://gitlab.com" \
    --set runnerRegistrationToken="$GITLAB_RUNNER_TOKEN" \
    --set rbac.create=true \
    --set rbac.clusterWideAccess=true gitlab/gitlab-runner
    ```

The output is similar to the following:

    "NAME: gitlab-runner
    LAST DEPLOYED: Mon Feb  8 23:37:48 2021
    NAMESPACE: gitlab
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    Your GitLab Runner should now be registered against the GitLab instance reachable at: ""https://www.gitlab.com"""

1.  Wait until the Gitlab runner is Ready.

    ```bash
    kubectl --context=${KCC_GKE} -n gitlab wait --for=condition=available deployment gitlab-runner-gitlab-runner --timeout=5m
    ```

The output is similar to the following:

    deployment.apps/gitlab-runner-gitlab-runner condition met

1.  List the runner in the group. You see the runner you just deployed.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

### Creating Gitlab projects

In this section, you create the following Gitlab subgroups and projects inside the main Gitlab group.

- _platform-admins-group_ - This subgroup contains repos belonging and owned by the platform admins team.
- _bank-of-anthos-group_ - This subgroup contains the repos belonging and owned by the Bank of Anthos application team.
- _online-boutique-group_ - This subgroup contains the repos belonging and owned by the Online Boutique applications team.
- _infra_ - This repo contains [Iac](https://en.wikipedia.org/wiki/Infrastructure_as_code) to build GCP resources for example projects, networks, GKE clusters etc. This repo is created in the `platform-admins-group` subgroup.
- _config_ - This is the Anthos Config Management or [ACM](https://cloud.google.com/anthos/config-management) repo used for deploying applications and policies to the GKE clusters. This repo is created in the `platform-admins-group` subgroup.
- _shared_cd_ - This is the repo that contains CD jobs which are used for deploying applications and policies to the GKE clusters. This repo is created in the `platform-admins-group` subgroup.
- _bank of anthos repos_ - These are a collection of repos for a demo application containing the source code and service manifests (Kubernetes manifests) for the Bank of Anthos application. These repos are created in the `bank-of-anthos-group` subgroup.
- _online boutique repos_ - These are a collection of repos for a demo application containing the source code and service manifests (Kubernetes manifests) for the Online Boutique application. This repo is created in the `online-boutique-group` subgroup.

1.  Create the Gitlab subgroups and repos (or projects) inside the main Gitlab group created in the previous step.

    ```bash
    export CREATE_REPO_SCRIPT="${WORKDIR}/asm-shared-vpc-single-project-in-workdir/admin/scripts/create_gitlab_repo.sh"
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_INFRA_PROJECT_NAME} --varprefix INFRA
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_CONFIG_PROJECT_NAME} --varprefix CONFIG
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_CD_PROJECT_NAME} --varprefix CD
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME} --varprefix BANK_ACCOUNTS_DB
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANK_LEDGER_DB_PROJECT_NAME} --varprefix BANK_LEDGER_DB
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_CONTACTS_PROJECT_NAME} --varprefix CONTACTS
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANKFRONTEND_PROJECT_NAME} --varprefix BANKFRONTEND
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BALANCEREADER_PROJECT_NAME} --varprefix BALANCEREADER
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_LEDGERWRITER_PROJECT_NAME} --varprefix LEDGERWRITER
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANKLOADGENERATOR_PROJECT_NAME} --varprefix BANKLOADGENERATOR
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_TRANSACTIONHISTORY_PROJECT_NAME} --varprefix TRANSACTIONHISTORY
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_USERSERVICE_PROJECT_NAME} --varprefix USERSERVICE
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_AD_PROJECT_NAME} --varprefix AD
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CART_PROJECT_NAME} --varprefix CART
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CHECKOUT_PROJECT_NAME} --varprefix CHECKOUT
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CURRENCY_PROJECT_NAME} --varprefix CURRENCY
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_EMAIL_PROJECT_NAME} --varprefix EMAIL
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOPFRONTEND_PROJECT_NAME} --varprefix SHOPFRONTEND
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOPLOADGENERATOR_PROJECT_NAME} --varprefix SHOPLOADGENERATOR
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_PAYMENT_PROJECT_NAME} --varprefix PAYMENT
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_PRODUCTCATALOG_PROJECT_NAME} --varprefix PRODUCTCATALOG
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_RECOMMENDATION_PROJECT_NAME} --varprefix RECOMMENDATION
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHIPPING_PROJECT_NAME} --varprefix SHIPPING
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOP_REDIS_PROJECT_NAME} --varprefix SHOP_REDIS
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_TEST_PROJECT_NAME} --varprefix SHOP_TEST
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_DATABASES_SUBGROUP_NAME} --repo ${GITLAB_COCKROACHDB_PROJECT_NAME} --varprefix COCKROACHDB
    ${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_DATABASES_SUBGROUP_NAME} --repo ${GITLAB_REDIS_PROJECT_NAME} --varprefix REDIS
    source ${WORKDIR}/vars.sh
    ```

1.  Create variables in the main Gitlab group which can be used by all Gitlab projects inside this group. These variables are used for Gitlab CI pipelines.

    ```bash
    cat ${WORKDIR}/vars.sh | base64 > ${WORKDIR}/vars-base64.txt
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ALL_VARIABLES" --form "value=$(cat ${WORKDIR}/vars-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${WORKDIR}/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_ACM_KEY" --form "value=$(cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ADMIN_USER" --form "value=${ADMIN_USER}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GROUP_NAME" --form "value=${GITLAB_GROUP_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_ADMIN_SUBGROUP_NAME" --form "value=${GITLAB_ADMIN_SUBGROUP_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_BANK_SUBGROUP_NAME" --form "value=${GITLAB_BANK_SUBGROUP_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_SHOP_SUBGROUP_NAME" --form "value=${GITLAB_SHOP_SUBGROUP_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=MAIN_FOLDER_ID" --form "value=${MAIN_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=TERMINAL_IP" --form "value=${TERMINAL_IP}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_CHANNEL" --form "value=${ASM_CHANNEL}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_LABEL" --form "value=${ASM_LABEL}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_VERSION" --form "value=${ASM_VERSION}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ACM_VERSION" --form "value=${ACM_VERSION}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=DOUBLE_QUOTES" --form "value=\"" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/groups/"${GITLAB_GROUP_ID}"/variables | jq
    ```

## Deploying Infrastructure

1.  Clone the `infrastructure` repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_INFRA_PROJECT_SSH_URL ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
    ```

1.  Copy the contents from the ASM repo into your `infrastructure` repo.

    ```bash
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/infrastructure/. ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
    git add .
    git commit -m "initial commit"
    git push
    ```

## Inspecting pipelines

In this section, you inspect the Gitlab pipelines which deploy resources in your GCP organization.

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    echo -n "${GITLAB_INFRA_PROJECT_WEB_URL}/-/pipelines"
    ```

<img src="admin/docs/infra-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.

> This pipeline takes about 25 to 30 minutes to complete.

## Pipeline Stages

This section briefly explains what each stage in the pipeline accomplishes.

### Folder

This stage creates a folder where all of the projects are created. Creating a folder makes it easier to organize resources for this guide.

### Projects

This stage creates four projects. One project is the host network project owned by the network admin team where they create a shared VPC and all networking resources for the organization. The second project is the platform admins project where all of the GKE clusters are maintained and managed. The last two projects are service projects belonging to the application teams -- `bank` and `shop`. Each application team gets their own GCP project. All projects use the shared VPC for network resources.

### Network

This stage creates a VPC network in the host network project. It also creates four subnets and secondary ranges required for the GKE clusters. Two subnets created in two different regions and are shared with the platrform admins project for the GKE clusters. Two more subnets are created and shared with the bank and shop projects.

### SharedVPC

This stage configures the shared VPC. Host network project is configured as the host project and the other three projects are added as service projects. Platform admins service project is allowed two subnets (in two regiojns) for GKE clusters. The other two service projects (bank and shop) are allowed one subnet that they can use for resources in that project.

### CloudNAT

This stage creates Cloud Nat gateways in the two regions. Cloud Nat gateways are used by private GKE clusters so that the Pods running inside GKE clusters can access the internet. This is also required by ASM, because the ASM controlplane Pods need access to all the GKE clusters' API servers. ASM controlplane Pods use Cloud Nat gateways in their region to access GKE API servers.

### GKE

This stage creates four GKE clusters. Two zonal clusters each are created in one region and the other two clusters are created in a different region. All four clusters are created in the platform admins project. The clusters are configured as private clusters. This means that the nodes do not get a public IP address. The clusters are also configured with `master-authorized-networks` enabled. This means that only specific CIDR IP ranges are allowed access to the GKE API servers. GKE clusters are configured with both region Cloud Nat gateway public IP addresses as well as the Gitlab CI runner public IP.

### PrivateGKEFirewallRules

This stage creates the appropriate firewall rules required for Pods to communicate to each other across multiple GKE clusters.

### HubService

This stage creates a [Anthos GKE Hub Service Account](https://cloud.google.com/anthos/multicluster-management/connect/prerequisites#gke-cross-project). In this architecture, you register the four clusters to the platform admin project; the same project that the clusters are provisioned in.

### ConfigureHub

This stage configures the Anthos GKE Hub service account IAM permissions so that the GKE clusters can register to the project.

### RegisterGKE

This stage registers the four clusters to the Anthos GKE hub project.

### ConfigureMCI

This stage enables the [Multicluster Ingress](https://cloud.google.com/kubernetes-engine/docs/concepts/multi-cluster-ingress) feature on the `ingress-config` cluster. This creates the `MulticlusterIngress` and `MulticlusterService` CRDs on the `ingress-config` cluster. You can then deploy the CRs to create multicluster ingress for your applications.

### EnableACM

This stage enables [Anthos Config Management](https://cloud.google.com/anthos/config-management) (ASM) on all clusters.

### ConfigureACM

This stage configures ACM on all clusters. You can use the `config` repo to deploy applications to the clusters. This stage also enables [Policy Controller](https://cloud.google.com/anthos-config-management/docs/concepts/policy-controller) on all clusters so that you can create, deploy and enforce policies.

### ASMPrepProjects

This stage runs the [initialize script](https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm#setting_credentials_and_permissions) to prepare the projects for ASM installation. The script runs in the `platform-admins` service project.

### ASMGKEInstall

This stage installs [ASM](https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm#installing_anthos_service_mesh) on all four GKE clusters.

### ASMMulticlusterServiceDiscovery

This stage creates [kubeconfig secrets](https://cloud.google.com/service-mesh/docs/gke-install-multi-cluster#configure_endpoint_discovery_between_clusters) for all GKE clusters in all GKE clusters so that the ASM controlplane can automatically discover and update Services and Endpoints.

### ASMVerifyMulticluster

This stage deploys the [`whereami`](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/whereami) and [`sleep`](https://github.com/istio/istio/tree/master/samples/sleep) apps in a `sample` namespace in all four clusters. The `sleep` app is a simple curl utility and the `whereami` app responds with metadata regarding where the service and Pods are running. The stage uses the `sleep` app to curl the `whereami` Pods running in all four clusters from all four clusters to ensure ASM multicluster functionality.

## Accessing clusters

1. Get access to the four GKE clusters.

   ```bash
   export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
   [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
   echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
   curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=FOLDER_ID" --form "value=${FOLDER_ID}" --form "protected=true"
   curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=INFRA_FOLDER_ID" --form "value=${FOLDER_ID}" --form "protected=true"
   gcloud config set project ${SVC_1_PROJECT}-${FOLDER_ID}

   gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE5_INGRESS} --zone ${GKE5_INGRESS_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}

   kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE1_ZONE}_${GKE1}
   kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE2_ZONE}_${GKE2}
   kubectl ctx ${GKE3}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE3_ZONE}_${GKE3}
   kubectl ctx ${GKE4}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE4_ZONE}_${GKE4}
   kubectl ctx ${GKE5_INGRESS}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE5_INGRESS_ZONE}_${GKE5_INGRESS}
   ```

> If your terminal's public IP address changes, you will lose access to the GKE clusters. You will need to update the [`authorized networks`](https://cloud.google.com/kubernetes-engine/docs/how-to/authorized-networks) on all 5 clusters with your terminal's new public IP. You can get your terminal's public IP from `curl ifconfig.me`.

## Enable Monitoring Workspace

Enable [Monitoring Workspace]() in the `ops` project prior to bootstrapping repos.

1.  Click on the following link:

    ```bash
    echo -e "\e[95mhttps://console.cloud.google.com/monitoring/signup?project=${SVC_1_PROJECT}-${FOLDER_ID}&timeDomain=1h&nextPath=monitoring\e[0m"
    ```

> It takes a few minutes to initialize the workspace.

## Bootstrapping admin repos

1.  Bootstrap `shared-cd` and `config` repos.

    ```bash
    mkdir -p ${WORKDIR}/repos
    cd ${WORKDIR}/repos
    # bootstrap shared-cd repo
    [ ! -d ${WORKDIR}/repos/shared-cd ] && git clone ${GITLAB_CD_PROJECT_SSH_URL} ${WORKDIR}/repos/shared-cd
    cd ${WORKDIR}/repos/shared-cd && git pull
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/shared-cd/. ${WORKDIR}/repos/shared-cd/.
    cd ${WORKDIR}/repos/shared-cd
    git add .
    git commit -m "initial commit"
    git push

    # Create git-creds-secrets for individual namespaces
    kubectl create secret generic git-creds \
      --from-file=ssh=${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key --dry-run=client -oyaml > ${WORKDIR}/tmp/secret-git-creds.yaml

    # bootstrap config repo
    [ ! -d ${WORKDIR}/repos/config ] && git clone ${GITLAB_CONFIG_PROJECT_SSH_URL} ${WORKDIR}/repos/config
    cd ${WORKDIR}/repos/config && git pull
    rm -rf ${WORKDIR}/repos/config/namespaces
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/config/. ${WORKDIR}/repos/config/.
    for app in bank-of-anthos online-boutique cockroachdb redis
    do
        cd ${WORKDIR}/repos/config/namespaces/${app}
        for FOLDER in $(ls -d * | xargs -n 1 echo)
        do
            export FOLDER=$FOLDER
            for f in $(find ${WORKDIR}/repos/config/namespaces/${app}/${FOLDER} -regex '.*namespace\.yaml_tmpl'); do envsubst < $f > "${WORKDIR}/repos/config/namespaces/${app}/${FOLDER}/$(basename ${f%.yaml_tmpl}.yaml)"; done
            rm -rf ${WORKDIR}/repos/config/namespaces/${app}/${FOLDER}/*.yaml_tmpl
        done
    done
    cd ${WORKDIR}/repos/config
    git pull
    git add .
    git commit -m "initial commit"
    git push
    ```

1.  Get `nomos` CLI and verify ACM status.

    ```bash
    cd ${WORKDIR}/tmp
    gsutil cp gs://config-management-release/released/1.9.1/linux_amd64/nomos nomos
    chmod +x nomos
    export NOMOS_191=${WORKDIR}/tmp/nomos
    # Verify nomos status
    ${NOMOS_191} status
    ```

## Bootstrapping database repos

1.  Bootstrap the `cockroachdb` and `redis` database repos. `redis` database is used to store cart items in the Online Boutqiue application. `cockroachdb` DB is used by Bank of Anthos application for users, accounts, transactions, ledger and balances.

    ```bash
    # CockroachDB
    export repo=cockroachdb
    [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_DATABASES_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
    cd ${WORKDIR}/repos/${repo} && git pull
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/databases/${repo}/. ${WORKDIR}/repos/${repo}/.
    cd ${WORKDIR}/repos/${repo}
    envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
    rm -rf .gitlab-ci.yml_tmpl
    git add .
    git commit -m "initial commit"
    git push || true

    # RedisDB
    export repo=redis
    [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_DATABASES_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
    cd ${WORKDIR}/repos/${repo} && git pull
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/databases/${repo}/. ${WORKDIR}/repos/${repo}/.
    cd ${WORKDIR}/repos/${repo}
    envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
    rm -rf .gitlab-ci.yml_tmpl
    git add .
    git commit -m "initial commit"
    git push || true
    ```

Inspect the pipelines and verify that databases are properly configured and hydrated (in the case of cockroachdb) prior to deploying the Online Boutique and Bank of Anthos applications.

1.  Inspect the two databases' deployment pipelines. Wait until both pipelines finish successfully.

    ```bash
    # cockroachdb pipeline
    echo -e "${GITLAB_COCKROACHDB_PROJECT_WEB_URL}/-/pipelines"
    # redisdb pipeline
    echo -e "${GITLAB_REDIS_PROJECT_WEB_URL}/-/pipelines"
    ```

1.  Once the pipelines finish successfully, ensure that StatefulSet Pods for both databases are _Ready_ on all clusters.

    ```bash
    # cockroachdb and redisdb
    for cluster in $GKE1 $GKE2 $GKE3 $GKE4
    do
      cluster_num=$(echo $cluster | cut -d '-' -f 2)
      kubectl --context=$cluster -n cockroachdb rollout status --watch --timeout=600s statefulset/gke${cluster_num}-cockroachdb
      kubectl --context=$cluster -n redis rollout status --watch --timeout=600s statefulset/gke${cluster_num}-redisdb
    done
    ```

1.  Check the `cluster nodes` to ensure 8 nodes are configured for the cluster.

    ```bash
    kubectl --context=$GKE1 -n redis exec -t gke1-redisdb-0 -- redis-cli cluster nodes
    ```

Output looks like the following:

    7867b814424ed35fb9ba91de7af115a7c34f89aa 240.0.0.11:6379@16379 master - 0 1615761392000 3 connected 8192-12287
    09d0e305f2f5455d5cbae5fd95bf1158cdf8ef39 240.0.0.31:6379@16379 master - 0 1615761391000 4 connected 12288-16383
    42fce19f740d6fe01747a91a3f1b3cf32476c9b6 240.0.0.41:6379@16379 slave 7867b814424ed35fb9ba91de7af115a7c34f89aa 0 1615761393000 3 connected
    6ed736fd3c4def98fe7aa5203266c9e66c9b5849 240.0.0.20:6379@16379 slave 09d0e305f2f5455d5cbae5fd95bf1158cdf8ef39 0 1615761393191 4 connected
    472c6a0a2d1863d4d303eb31bb1acb5a4f7fa582 240.0.0.21:6379@16379 slave 1eb499c47af76214547752196303641e6cfdd485 0 1615761394194 2 connected
    1eb499c47af76214547752196303641e6cfdd485 240.0.0.30:6379@16379 master - 0 1615761395246 2 connected 4096-8191
    05511a36dbff9a2797117c73145e80f5d8f067fd 240.0.0.40:6379@16379 slave 4e2eb2cfe04af1b0b98002709464d59c0ab2228a 0 1615761393000 1 connected
    4e2eb2cfe04af1b0b98002709464d59c0ab2228a 240.0.0.10:6379@16379 myself,master - 0 1615761392000 1 connected 0-4095

There are 8 nodes in the Redis cluster. 2 statefulset Pods are deployed in each of the 4 GJKE clusters. Note the 240.0.0.X IP addresses. These are created using [ServiceEntrys](https://istio.io/latest/docs/reference/config/networking/service-entry/) in ASM which provide a stable static IP address to each node. In Redis, you must configure nodes via IP address. Hostnames are not used at this time.

1.  For CockroachDB, check the the two DBs (`accountsdb` and `postgresdb`) used by Bank of Anthos application are present and hydrated with data.

    ```bash
    kubectl --context ${GKE1} -n cockroachdb exec gke1-cockroachdb-0 -c cockroachdb -- cockroach sql --insecure --host=cockroachdb --execute="show databases;" | grep -e accountsdb -e postgresdb
    ```

Output looks like the following:

    accountsdb
    postgresdb

1.  Ensure there is data in the two databases.

    ```bash
    kubectl --context ${GKE1} -n cockroachdb exec gke1-cockroachdb-0 -c cockroachdb -- cockroach sql --insecure --host=cockroachdb --execute "use accountsdb;select count(*) from users"
    kubectl --context ${GKE1} -n cockroachdb exec gke1-cockroachdb-0 -c cockroachdb -- cockroach sql --insecure --host=cockroachdb --execute "use postgresdb;select count(*) from transactions"
    ```

Output looks like the following:

    3635
    38304

> The count may be different.

## Deploying Bank of Anthos

In this section, you deploy Bank of Anthos to the GKE clusters using multiple Gitlab project/repos and Gitlab CI.
Bank of Anthos is composed of multiple services. A Gitlab subgroup called `bank-of-anthos-group` is created. Within this subgroup, each Service gets it own Gitlab repo. Each repo consists of both the source code and the service manifests to build and deploy the service.

1.  Run the following script which sets up each repo with the source code, service manifests and CI pipelines.

    ```bash
    # Bank of Anthos application repo
    bank_repos=(
        $GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME
        $GITLAB_BANK_LEDGER_DB_PROJECT_NAME
        $GITLAB_CONTACTS_PROJECT_NAME
        $GITLAB_BANKFRONTEND_PROJECT_NAME
        $GITLAB_BALANCEREADER_PROJECT_NAME
        $GITLAB_LEDGERWRITER_PROJECT_NAME
        $GITLAB_BANKLOADGENERATOR_PROJECT_NAME
        $GITLAB_TRANSACTIONHISTORY_PROJECT_NAME
        $GITLAB_USERSERVICE_PROJECT_NAME
    )
    for repo in "${bank_repos[@]}"
    do
        echo -e "\e[95mBuilding $repo repo...\e[0m"
        [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_BANK_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
        cd ${WORKDIR}/repos/${repo} && git pull
        cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/bank-of-anthos/${repo}/. ${WORKDIR}/repos/${repo}/.
        cd ${WORKDIR}/repos/${repo}
        envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
        rm -rf .gitlab-ci.yml_tmpl
        git add .
        git commit -m "initial commit"
        git push || true
        echo -e "\e[95mSleeping for 15 seconds in between builds to avoid hitting Cloud Build API rate limit...\e[0m"
        sleep 15
    done
    ```

Following the script, every repo kicks of a pipeline which builds the container image (and stores it in GCR), builds the Kubernetes manifests using [kustomize](kustomize.io), and commits the _hydrated_ Kubernetes manifests to the ACM repo. The ACM repo keeps the Kubernetes manifests in sync with the clusters.

1. You can access the Gitlab CI pipelines for the invididual repos by navigating to the following links.

   ```bash
   # Balancereader
   echo -e "${GITLAB_BALANCEREADER_PROJECT_WEB_URL}/-/pipelines"
   # Contacts
   echo -e "${GITLAB_CONTACTS_PROJECT_WEB_URL}/-/pipelines"
   # Bankfrontend
   echo -e "${GITLAB_BANKFRONTEND_PROJECT_WEB_URL}/-/pipelines"
   # Ledgerwriter
   echo -e "${GITLAB_LEDGERWRITER_PROJECT_WEB_URL}/-/pipelines"
   # Transactionhistory
   echo -e "${GITLAB_TRANSACTIONHISTORY_PROJECT_WEB_URL}/-/pipelines"
   # Userservice
   echo -e "${GITLAB_USERSERVICE_PROJECT_WEB_URL}/-/pipelines"
   # Bankloadgenerator
   echo -e "${GITLAB_BANKLOADGENERATOR_PROJECT_WEB_URL}/-/pipelines"
   # Accounts DB - This is not used in this setup. insetad CockroachDB is used. However, it is deployed and can be used.
   echo -e "${GITLAB_BANK_ACCOUNTS_DB_PROJECT_WEB_URL}/-/pipelines"
   # Ledger DB - This is not used in this setup. insetad CockroachDB is used. However, it is deployed and can be used.
   echo -e "${GITLAB_BANK_LEDGER_DB_PROJECT_WEB_URL}/-/pipelines"
   ```

Wait until all pipelines are successfully finished.

You can click on individual stages and monitor progress of all the steps within each stage.

> Each pipeline takes about 5 - 10 minutes to complete. The `bankfrontend` pipeline take 10 - 15 minutes to complete.

### Accessing Bank of Anthos through Multicluster Ingress

Bank of Anthos application is accessed via the `bankfrontend` Service. `bankfrontend` is a Python flask application that access all other services as backends. In this setup, `bankfrontend` Service runs in all clusters (just like every other Service. [Multicluster Ingress](https://cloud.google.com/solutions/exposing-service-mesh-apps-through-gke-ingress) is used to create a [GCLB](https://cloud.google.com/load-balancing) which sends client traffic to the `istio-ingressgateway` Services running in all four clusters. A [VirtualService](https://istio.io/latest/docs/reference/config/networking/virtual-service/) is created which looks at the Host Header field for incoming requests in to the ASM mesh and routes the traffic to the `bankfrontend` Services in all four clusters. Multicluster ingress and multicluster Services are much more resilient since they do not solely rely on individual Kubernetes infrastructure for up time. In this setup, you can lose all but one cluster and your Service (maybe degraded) will still not be down.

### Securing Ingress

To create end to end secure ingress, a [Google managed certificate](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs) is used at the GCLB. Google managed certificates are public certificates and are free to use with GKE services. In order to create a Google managed certificate, you need a DNS name. In this setup, you use [Cloud Endpoints](https://cloud.google.com/endpoints/docs/openapi/cloud-goog-dns-configure) to create a free DNS name, which is used to access the `bankfrontend` Service. Google managed certificates can take up to 30 minutes to be provisioned. You will not be able to access Bank of Anthos until the managed certificate has been provisioned.

1.  Verify that the managed certificate is provisioned.

    ```bash
    gcloud --project ${SVC_1_PROJECT}-${FOLDER_ID} compute ssl-certificates list
    ```

The output looks like the following.

    NAME TYPE CREATION_TIMESTAMP EXPIRE_TIME MANAGED_STATUS

    bankfrontend-ingress-cert MANAGED 2021-02-13T11:49:41.212-08:00 2021-05-14T11:59:47.000-07:00 ACTIVE
    bank.endpoints.proj-1-ops-prod-1049181712246.cloud.goog: ACTIVE

> Note that the _MANAGED_STATUS_ field changes to _ACTIVE_. You may have to run this command multiple times until the status shows _ACTIVE_.

1.  Once the certificate is _ACTIVE_, you can access Bank of Anthos by navigating to the following link.

    ```bash
    echo -e "https://bank.endpoints.${SVC_1_PROJECT}-${FOLDER_ID}.cloud.goog"
    ```

Access the application by navigating to the link, login, create a deposit and transfer funds to other accounts. You can also create a new user and login using the new user and perform transactions. All features/services of the applications should be functional.

&nbsp;
<img src="admin/docs/bank-frontend.png" width=90% height=90%>
&nbsp;

## Deploying Online Boutique

> Deploy the Bank of Anthos prior to deploying Online Boutique application. Otherwise, you may run into Cloud Build quota limits for running too many build jobs concurrently. If that does happen, you can rerun the specific pipelines that failed.

In this section, you deploy Online Boutique to the GKE clusters using multiple Gitlab project/repos and Gitlab CI.
Online Boutique is composed of multiple services. A Gitlab subgroup called `online-boutique-group` is created. Within this subgroup, each Service gets it own Gitlab repo. Each repo consists of both the source code and the service manifests to build and deploy the service.

1.  Run the following script which sets up each repo with the source code, service manifests and CI pipelines.

    ```bash
    # Online Boutique application repo
    shop_repos=(
        $GITLAB_AD_PROJECT_NAME
        $GITLAB_CART_PROJECT_NAME
        $GITLAB_CHECKOUT_PROJECT_NAME
        $GITLAB_CURRENCY_PROJECT_NAME
        $GITLAB_EMAIL_PROJECT_NAME
        $GITLAB_SHOPFRONTEND_PROJECT_NAME
        $GITLAB_SHOPLOADGENERATOR_PROJECT_NAME
        $GITLAB_PAYMENT_PROJECT_NAME
        $GITLAB_PRODUCTCATALOG_PROJECT_NAME
        $GITLAB_RECOMMENDATION_PROJECT_NAME
        $GITLAB_SHIPPING_PROJECT_NAME
        $GITLAB_SHOP_REDIS_PROJECT_NAME
        $GITLAB_TEST_PROJECT_NAME
    )
    for repo in "${shop_repos[@]}"
    do
        echo -e "\e[95mBuilding $repo repo...\e[0m"
        [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_SHOP_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
        cd ${WORKDIR}/repos/${repo} && git pull
        cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/online-boutique/${repo}/. ${WORKDIR}/repos/${repo}/.
        cd ${WORKDIR}/repos/${repo}
        envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
        rm -rf .gitlab-ci.yml_tmpl
        git add .
        git commit -m "initial commit"
        git push || true
        echo -e "\e[95mSleeping for 15 seconds in between builds to avoid hitting Cloud Build API rate limit...\e[0m"
        sleep 15
    done
    ```

Following the script, every repo kicks of a pipeline which builds the container image (and stores it in GCR), builds the Kubernetes manifests using [kustomize](kustomize.io), and commits the _hydrated_ Kubernetes manifests to the ACM repo. The ACM repo keeps the Kubernetes manifests in sync with the clusters.

1. You can access the Gitlab CI pipelines for the invididual repos by navigating to the following links.

   ```bash
   # Ad
   echo -e "${GITLAB_AD_PROJECT_WEB_URL}/-/pipelines"
   # Cart
   echo -e "${GITLAB_CART_PROJECT_WEB_URL}/-/pipelines"
   # Checkout
   echo -e "${GITLAB_CHECKOUT_PROJECT_WEB_URL}/-/pipelines"
   # Currency
   echo -e "${GITLAB_CURRENCY_PROJECT_WEB_URL}/-/pipelines"
   # Email
   echo -e "${GITLAB_EMAIL_PROJECT_WEB_URL}/-/pipelines"
   # Payment
   echo -e "${GITLAB_PAYMENT_PROJECT_WEB_URL}/-/pipelines"
   # Product catalog
   echo -e "${GITLAB_PRODUCTCATALOG_PROJECT_WEB_URL}/-/pipelines"
   # Recommendation
   echo -e "${GITLAB_RECOMMENDATION_PROJECT_WEB_URL}/-/pipelines"
   # Shipping
   echo -e "${GITLAB_SHIPPING_PROJECT_WEB_URL}/-/pipelines"
   # Shopfrontend
   echo -e "${GITLAB_SHOPFRONTEND_PROJECT_WEB_URL}/-/pipelines"
   # Shoploadgenerator
   echo -e "${GITLAB_SHOPLOADGENERATOR_PROJECT_WEB_URL}/-/pipelines"
   # Redis DB - This is not used in this setup. Insetad a Redis cluster is used. However, this is a simple Deployment and can be used as a standalone single node Redis.
   echo -e "${GITLAB_SHOP_REDIS_PROJECT_WEB_URL}/-/pipelines"
   # Test
   echo -e "${GITLAB_SHOP_TEST_PROJECT_WEB_URL}/-/pipelines"
   ```

Wait until all pipelines are successfully finished.

You can click on individual stages and monitor progress of all the steps within each stage.

> Each pipeline takes about 5 - 10 minutes to complete. The `shopfrontend` pipeline take 10 - 15 minutes to complete.

### Accessing Online Boutique through Multicluster Ingress

Online Boutique application is accessed via the `shopfrontend` Service. `shopfrontend` is a Go web application . In this setup, `shopfrontend` Service runs in all clusters (just like every other Service. [Multicluster Ingress](https://cloud.google.com/solutions/exposing-service-mesh-apps-through-gke-ingress) is used to create a [GCLB](https://cloud.google.com/load-balancing) which sends client traffic to the `istio-ingressgateway` Services running in all four clusters. A [VirtualService](https://istio.io/latest/docs/reference/config/networking/virtual-service/) is created which looks at the Host Header field for incoming requests in to the ASM mesh and routes the traffic to the `shopfrontend` Services in all four clusters. Multicluster ingress and multicluster Services are much more resilient since they do not solely rely on individual Kubernetes infrastructure for up time. In this setup, you can lose all but one cluster and your Service (maybe degraded) will still not be down.

### Securing Ingress

To create end to end secure ingress, a [Google managed certificate](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs) is used at the GCLB. Google managed certificates are public certificates and are free to use with GKE services. In order to create a Google managed certificate, you need a DNS name. In this setup, you use [Cloud Endpoints](https://cloud.google.com/endpoints/docs/openapi/cloud-goog-dns-configure) to create a free DNS name, which is used to access the `shopfrontend` Service. Google managed certificates can take up to 30 minutes to be provisioned. You will not be able to access Online Boutique until the managed certificate has been provisioned.

1.  Verify that the managed certificate is provisioned.

    ```bash
    gcloud --project ${SVC_1_PROJECT}-${FOLDER_ID} compute ssl-certificates list
    ```

The output looks like the following.

    NAME TYPE CREATION_TIMESTAMP EXPIRE_TIME MANAGED_STATUS

    shopfrontend-ingress-cert MANAGED 2021-02-13T11:49:41.212-08:00 2021-05-14T11:59:47.000-07:00 ACTIVE
    shop.endpoints.proj-1-ops-prod-1049181712246.cloud.goog: ACTIVE

> Note that the _MANAGED_STATUS_ field changes to _ACTIVE_. You may have to run this command multiple times until the status shows _ACTIVE_.

1.  Once the certificate is _ACTIVE_, you can access Online Boutique by navigating to the following link.

    ```bash
    echo -e "https://shop.endpoints.${SVC_1_PROJECT}-${FOLDER_ID}.cloud.goog"
    ```

Access the application by navigating to the link. You can browse items, place them in your cart, get recommendations based on your browsing and cart items and conduct a checkout.

&nbsp;
<img src="admin/docs/shop-frontend.png" width=90% height=90%>
&nbsp;

## Cleaning up

To avoid incurring charges to your Google Cloud Platform account for the resources used in this tutorial.

You can delete all the resources, the four projects and the folder you created for this guide. You can also delete the KCC folder and the `proj-infra-admin` project.
You must delete the Cloud Endpoints services you created for the `bank` and `shop` applications before you can delete the `platform-admins` service project.
When you create a shared VPC, a [lien](https://cloud.google.com/resource-manager/docs/project-liens) is placed on the network host project. The lien must be removed prior to deleting the network host project. The two service projects can be deleted.

1.  Define your `WORKDIR` variables, if you do not have one already defined.

    ```bash
    # cd into your WORKDIR
    export WORKDIR=`pwd`
    ```

1.  Delete the Cloud Endpoints services in the `platform-admins` service project.

    ```bash
    source $WORKDIR/vars.sh
    export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
    [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"

    gcloud endpoints services delete bank.endpoints.${SVC_1_PROJECT}-${FOLDER_ID}.cloud.goog \
    --project ${SVC_1_PROJECT}-${FOLDER_ID} --async --quiet

    gcloud endpoints services delete shop.endpoints.${SVC_1_PROJECT}-${FOLDER_ID}.cloud.goog \
    --project ${SVC_1_PROJECT}-${FOLDER_ID} --async --quiet
    ```

1.  Remove the lien from the network host project.

    ```bash
    export LIEN_ID=$(gcloud alpha resource-manager liens list --project="${HOST_NET_PROJECT}-${FOLDER_ID}" | awk 'NR==2 {print $1}')
    gcloud alpha resource-manager liens delete $LIEN_ID
    ```

The output is similar to the following:

    Deleted [liens/p448184974609-lca24ae52-9480-44e9-877d-db8dd7020ad7].

1.  Delete the four projects. You can undelete projects within 30 days of deletion.

    ```bash
    gcloud projects delete "${SVC_1_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_2_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_3_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${HOST_NET_PROJECT}-${FOLDER_ID}" --quiet
    ```

The output is similar to the following:

    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-1-ops-prod-814581352178].
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-2-bank-prod-814581352178].
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-3-shop-prod-814581352178].
    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-0-net-prod-814581352178].

1.  Delete the `infra` folder

    ```bash
    gcloud resource-manager folders delete ${FOLDER_ID}
    ```

The output is similar to the following:

    Deleted [<Folder
     createTime: '2021-03-14T20:36:48.183Z'
     displayName: 'infra'
     lifecycleState: LifecycleStateValueValuesEnum(DELETE_REQUESTED, 2)
     name: 'folders/732851712019'
     parent: 'folders/197596596558'>].

1.  Delete the `proj-infra-admin` project.

    ```bash
    gcloud projects delete "${KCC_PROJECT}-${KCC_FOLDER_ID}" --quiet
    ```

1.  Delete the KCC folder.

    ```bash
    gcloud resource-manager folders delete ${KCC_FOLDER_ID}
    ```

1.  Unset `KUBECONFIG`.

    ```bash
    unset KUBECONFIG
    ```

1.  Remove the KCC SA and Gitlab CI SA Organization IAM roles.

    ```bash
    kcc_sa_iam_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
        roles/accesscontextmanager.policyAdmin
    )
    for role in "${kcc_sa_iam_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done

    gitlab_sa_iam_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
        roles/accesscontextmanager.policyAdmin
    )
    for role in "${gitlab_sa_iam_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done
    ```

1.  Delete the main folder.

    ```bash
    gcloud resource-manager folders delete ${MAIN_FOLDER_ID}
    ```

1.  Delete the main Gitlab group. Deleting the main group also deletes the subgroups and all of the projects inside.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID"
    ```

The output is similar to the following:

    {"message":"202 Accepted"}

1.  Delete the Gitlab SSH key.

    ```bash
    export GITLAB_SSH_KEY_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/keys | jq --arg GITLAB_SSH_KEY "${GITLAB_SSH_KEY}" '.[] | select(.title==$GITLAB_SSH_KEY)' | jq -r '.id')

    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/user/keys/$GITLAB_SSH_KEY_ID"
    ```

1.  Remove KCC SA as a billing user from your billing account.

    ```bash
    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) -= ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > $WORKDIR/billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} $WORKDIR/billing-iam-policy.json
    ```

1.  Delete `WORKDIR` and unset variables.

    ```bash
    cd $WORKDIR/..
    rm -rf $WORKDIR

    unset FOLDER_ID
    unset MAIN_FOLDER_ID
    unset WORKDIR

    gcloud config unset project
    ```

1.  Optionally, you can also revoke the API personal access token by navigating to the following page.

    ```bash
    echo -n "https://gitlab.com/-/profile/personal_access_tokens"
    ```

## Appendix

Documentation used for this guide.

- https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-shared-vpc
- https://cloud.google.com/anthos/multicluster-management/connect/prerequisites
- https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
- https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project
- https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm
- https://cloud.google.com/kubernetes-engine/docs/how-to/troubleshooting-and-ops#avmbr110_iam_permission_permission_missing_for_cluster_name
