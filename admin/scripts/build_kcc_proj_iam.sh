#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Creating KCC GCP SA and assigning IAM roles..."
export KCC_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${KCC_SERVICE_ACCOUNT})
if [ ! "$KCC_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${KCC_SERVICE_ACCOUNT}

    project_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
        roles/accesscontextmanager.policyAdmin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done

    gcloud beta billing accounts add-iam-policy-binding ${BILLING_ACCOUNT} \
        --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role=roles/billing.user
fi

echo -e "\n"
title_no_wait "Creating GCP service account for Gitlab CI runner and asigning IAM roles..."
export GITLAB_GCP_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${GITLAB_CI_GCP_SA_NAME})
if [ ! "$GITLAB_GCP_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${GITLAB_CI_GCP_SA_NAME}

    project_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
        roles/accesscontextmanager.policyAdmin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done
fi

echo -e "\n"
title_no_wait "Creating Gitlab GCP service account credentials..."
if [ ! -f "$WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json" ]; then
    gcloud iam service-accounts keys create $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json \
    --iam-account ${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com
    cat $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json | base64 > $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt
fi

