#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

TOKEN=${GITLAB_TOKEN}
for REPO in ${GITLAB_BANK_ACCOUNTS_DB_PROJECT_ID} \
             ${GITLAB_BANK_LEDGER_DB_PROJECT_ID} \
             ${GITLAB_CONTACTS_PROJECT_ID} \
             ${GITLAB_BANKFRONTEND_PROJECT_ID} \
             ${GITLAB_BALANCEREADER_PROJECT_ID} \
             ${GITLAB_LEDGERWRITER_PROJECT_ID} \
             ${GITLAB_BANKLOADGENERATOR_PROJECT_ID} \
             ${GITLAB_TRANSACTIONHISTORY_PROJECT_ID} \
             ${GITLAB_USERSERVICE_PROJECT_ID}; do
    REPO_NAME=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO}" | jq -r '.name')
    STATUS=$(${SCRIPT_DIR}/get_gitlab_pipeline_status.sh -r ${REPO})
    URL=$(${SCRIPT_DIR}/get_gitlab_pipeline_url.sh -r ${REPO})
    title_no_wait "${REPO_NAME} pipeline status is ${STATUS}"
    title_no_wait "${REPO_NAME} pipeline URL is ${URL}"
done

