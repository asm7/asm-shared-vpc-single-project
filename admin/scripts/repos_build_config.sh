#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

# Create git-creds-secrets for individual namespaces
kubectl create secret generic git-creds \
--from-file=ssh=${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key --dry-run=client -oyaml > ${WORKDIR}/tmp/secret-git-creds.yaml

# bootstrap config repo
[ ! -d ${WORKDIR}/repos/config ] && git clone ${GITLAB_CONFIG_PROJECT_SSH_URL} ${WORKDIR}/repos/config
cd ${WORKDIR}/repos/config && git pull
rm -rf ${WORKDIR}/repos/config/namespaces
cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/config/. ${WORKDIR}/repos/config/.
for app in bank-of-anthos online-boutique cockroachdb redis
do
    cd ${WORKDIR}/repos/config/namespaces/${app}
    for FOLDER in $(ls -d * | xargs -n 1 echo)
    do
        export FOLDER=$FOLDER
        for f in $(find ${WORKDIR}/repos/config/namespaces/${app}/${FOLDER} -regex '.*namespace\.yaml_tmpl'); do envsubst < $f > "${WORKDIR}/repos/config/namespaces/${app}/${FOLDER}/$(basename ${f%.yaml_tmpl}.yaml)"; done
        rm -rf ${WORKDIR}/repos/config/namespaces/${app}/${FOLDER}/*.yaml_tmpl
    done
done
cd ${WORKDIR}/repos/config
git pull
git add .
git commit -m "initial commit"
git push || true

gsutil cp gs://config-management-release/released/1.6.1/linux_amd64/nomos ${WORKDIR}/tmp/nomos
chmod +x ${WORKDIR}/tmp/nomos
export NOMOS_161=${WORKDIR}/tmp/nomos
echo -e "export NOMOS_161=${WORKDIR}/tmp/nomos" >> $WORKDIR/vars.sh
# Verify nomos status
${NOMOS_161} status
