#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Getting the main Gitlab group runner..."
export GITLAB_RUNNER_TOKEN=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID" | jq -r '.runners_token')
[ -z "$GITLAB_RUNNER_TOKEN" ] && echo "GITLAB_RUNNER_TOKEN is not exported" || echo "GITLAB_RUNNER_TOKEN is $GITLAB_RUNNER_TOKEN"
echo -e "export GITLAB_RUNNER_TOKEN=$GITLAB_RUNNER_TOKEN" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Deploying Gitlab runner helm chart in the kcc cluster..."
helm repo add gitlab https://charts.gitlab.io

cat <<EOF > $WORKDIR/gitlab-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: gitlab
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/gitlab-namespace.yaml

export GITLAB_HELM_EXISTS=$(helm list -n gitlab -ojson | jq -r '.[].name')
if [ ! "$GITLAB_HELM_EXISTS" ]; then
    helm install --namespace gitlab gitlab-runner --set gitlabUrl="https://gitlab.com" \
    --set runnerRegistrationToken="$GITLAB_RUNNER_TOKEN" \
    --set rbac.create=true \
    --set rbac.clusterWideAccess=true gitlab/gitlab-runner
fi

kubectl --context=${KCC_GKE} -n gitlab wait --for=condition=available deployment gitlab-runner-gitlab-runner --timeout=5m

echo -e "\n"
title_no_wait "Getting the shared Gitlab group runner..."
curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
