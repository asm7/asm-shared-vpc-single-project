#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh


usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh


# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

TOKEN=${GITLAB_TOKEN}
for REPO in ${GITLAB_SHOPFRONTEND_PROJECT_ID} \
             ${GITLAB_SHOPLOADGENERATOR_PROJECT_ID} \
             ${GITLAB_PAYMENT_PROJECT_ID} \
             ${GITLAB_PRODUCTCATALOG_PROJECT_ID} \
             ${GITLAB_RECOMMENDATION_PROJECT_ID} \
             ${GITLAB_SHIPPING_PROJECT_ID} \
             ${GITLAB_SHOP_REDIS_PROJECT_ID} \
             ${GITLAB_AD_PROJECT_ID} \
             ${GITLAB_CART_PROJECT_ID} \
             ${GITLAB_CHECKOUT_PROJECT_ID} \
             ${GITLAB_CURRENCY_PROJECT_ID} \
             ${GITLAB_EMAIL_PROJECT_ID}; do
    REPO_NAME=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO}" | jq -r '.name')
    TRIGGER_TOKEN=$(curl -s --request POST --header "PRIVATE-TOKEN: ${TOKEN}" \
        --form description="my-api-trigger" "https://gitlab.com/api/v4/projects/${REPO}/triggers" | jq -r '.token')
    echo -e "\e[95mRetriggering pipeline for ${REPO_NAME} repo\e[0m"
    curl -s --request POST \
        --form token=${TRIGGER_TOKEN} \
        --form ref=main \
    "https://gitlab.com/api/v4/projects/${REPO}/trigger/pipeline" | jq -r '.web_url'
    echo -e "\e[95mSleeping for 15 seconds in between retriggers...\e[0m"
    sleep 15
done

