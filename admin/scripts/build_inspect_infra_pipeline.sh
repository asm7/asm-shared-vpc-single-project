#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh
echo -e "\n"
title_no_wait "Inspect the pipeline by clicking on the following link..."
echo -n "${GITLAB_INFRA_PROJECT_WEB_URL}/-/pipelines"
echo -e "\n"

export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/pipelines" | jq -r '.[0].status')
export INFRA_TRIES=0
until [[ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]] || [[ $INFRA_GITLAB_PIPELINE_STATUS = "success" ]]; do
    echo -e "\e[93mWaiting for infrastructure pipeline to get going..."
    sleep 5
    export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/pipelines" | jq -r '.[0].status')
    export INFRA_TRIES=$((INFRA_TRIES+1))
    if [ $INFRA_TRIES = 50 ]; then 
        echo -e "\e[91mInfrastructure pipeline is not starting. Exiting...\n"
        exit 1
    fi
done

if [ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]; then echo -e "\e[92mInfrastructure pipeline is running...\n"; fi
if [ $INFRA_GITLAB_PIPELINE_STATUS = "success" ]; then echo -e "\e[92mInfrastructure pipeline has finished successfully.\n"; fi


while [ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]; do
    echo -e "Successful jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=success" | jq '.[].name'
    echo -e "Running jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=running" | jq '.[].name'
    echo -e "Pending jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=pending" | jq '.[].name'
    echo -e "Upcoming jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=created" | jq '.[].name'
    echo -e "Gitlab pipeline is $INFRA_GITLAB_PIPELINE_STATUS, sleeping for 10 seconds..."
    sleep 10
    export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/pipelines" | jq -r '.[].status')
done

if [ $INFRA_GITLAB_PIPELINE_STATUS = "failed" ]; then 
    echo -e "\e[92mInfrastructure pipeline has failed!\n"
    echo -e "\e[92mStopping automatic build procedure.\n"
    exit 1
fi
