#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--workdir | -w The WORKDIR variable where the environment files are located."
   echo -e "\tExample usage:"
   echo -e "\t./cleanup.sh -w FULL_PATH_TO_WORKDIR"
   exit 1 # Exit script after printing help
}

# Setting default value
WORKDIR=${WORKDIR}

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --workdir | -w )              shift
                                      WORKDIR=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done


# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\n"
title_no_wait "*** CLEANUP ***"
echo -e "\n"

if [[ ! ${WORKDIR} ]]; then
    title_no_wait "You have not defined your WORKDIR variable."
    title_no_wait "Please run './cleanup.sh --help'."
    exit 1
fi

print_and_execute "source $WORKDIR/vars.sh"

if [ -z ${INFRA_FOLDER_ID} ]; then
  INFRA_FOLDER_ID=$(gcloud resource-manager folders list --folder="$MAIN_FOLDER_ID" --filter="display_name:${FOLDER_NAME}" --format='value(name)')
fi

# if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
#     print_and_execute "export FOLDER_ID=${INFRA_FOLDER_ID}"
#     [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" && exit 1 || echo "FOLDER_ID is $FOLDER_ID"
#     echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
# fi

echo -e "\n"
title_no_wait "Deleting Cloud Endpoint services..."
if gcloud projects list --filter "${SVC_1_PROJECT}-${INFRA_FOLDER_ID}" | grep "${SVC_1_PROJECT}-${INFRA_FOLDER_ID}"; then 
    export BANK_SERVICE_EXISTS=$(gcloud endpoints services --project=${SVC_1_PROJECT}-${INFRA_FOLDER_ID} list --format='value(serviceName)' | grep bank)
    export SHOP_SERVICE_EXISTS=$(gcloud endpoints services --project=${SVC_1_PROJECT}-${INFRA_FOLDER_ID} list --format='value(serviceName)' | grep shop)
    if [ ${BANK_SERVICE_EXISTS} ]; then
        print_and_execute "gcloud endpoints services delete bank.endpoints.${SVC_1_PROJECT}-${INFRA_FOLDER_ID}.cloud.goog --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID} --async --quiet"
    fi
    if [ ${SHOP_SERVICE_EXISTS} ]; then
        print_and_execute "gcloud endpoints services delete shop.endpoints.${SVC_1_PROJECT}-${INFRA_FOLDER_ID}.cloud.goog --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID} --async --quiet"
    fi
fi

echo -e "\n"
title_no_wait "Deleting host net project lien..."
print_and_execute "export LIEN_ID=$(gcloud alpha resource-manager liens list --project="${HOST_NET_PROJECT}-${FOLDER_ID}" --format='value(name)')"
[ -z "$LIEN_ID" ] && echo "LIEN_ID not found" || print_and_execute "gcloud alpha resource-manager liens delete $LIEN_ID"

echo -e "\n"
title_no_wait "Deleting svc1 project..."
if gcloud projects list --filter "${SVC_1_PROJECT}-${INFRA_FOLDER_ID}" | grep "${SVC_1_PROJECT}-${INFRA_FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${SVC_1_PROJECT}-${INFRA_FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting svc2 project..."
if gcloud projects list --filter "${SVC_2_PROJECT}-${INFRA_FOLDER_ID}" | grep "${SVC_2_PROJECT}-${INFRA_FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${SVC_2_PROJECT}-${INFRA_FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting svc3 project..."
if gcloud projects list --filter "${SVC_3_PROJECT}-${INFRA_FOLDER_ID}" | grep "${SVC_3_PROJECT}-${INFRA_FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${SVC_3_PROJECT}-${INFRA_FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting host net project..."
if gcloud projects list --filter "${HOST_NET_PROJECT}-${INFRA_FOLDER_ID}" | grep "${HOST_NET_PROJECT}-${INFRA_FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${HOST_NET_PROJECT}-${INFRA_FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting infra folder..."
if gcloud resource-manager folders list --organization="$ORG_ID" --filter "$INFRA_FOLDER_ID" | grep "$INFRA_FOLDER_ID"; then
  print_and_execute "gcloud resource-manager folders delete ${INFRA_FOLDER_ID}"
fi

echo -e "\n"
title_no_wait "Deleting KCC and Gitlab CI GCP service accounts organization level IAM roles..."
export KCC_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${KCC_SERVICE_ACCOUNT})
if [ "$KCC_SA_EXISTS" ]; then
    kcc_sa_iam_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
        roles/accesscontextmanager.policyAdmin
    )
    for role in "${kcc_sa_iam_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done

    gitlab_sa_iam_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
        roles/accesscontextmanager.policyAdmin
    )
    for role in "${gitlab_sa_iam_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done
fi

echo -e "\n"
title_no_wait "Deleting kcc project..."
if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${KCC_PROJECT}-${KCC_FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting kcc and infra folders..."
if gcloud resource-manager folders list --organization="$ORG_ID" --filter "${MAIN_FOLDER_ID}" | grep "${MAIN_FOLDER_ID}"; then
  if gcloud resource-manager folders list --folder="$MAIN_FOLDER_ID" --filter "$KCC_FOLDER_ID" | grep "$KCC_FOLDER_ID"; then
    print_and_execute "gcloud resource-manager folders delete ${KCC_FOLDER_ID}"
  fi
  if gcloud resource-manager folders list --folder="$MAIN_FOLDER_ID" --filter "$INFRA_FOLDER_ID" | grep "$INFRA_FOLDER_ID"; then
    print_and_execute "gcloud resource-manager folders delete ${INFRA_FOLDER_ID}"
  fi
fi

echo -e "\n"
title_no_wait "Deleting main folder..."
if gcloud resource-manager folders list --organization="$ORG_ID" --filter "${MAIN_FOLDER_ID}" | grep "${MAIN_FOLDER_ID}"; then
  print_and_execute "gcloud resource-manager folders delete ${MAIN_FOLDER_ID}"
fi

echo -e "\n"
title_no_wait "Deleting the main Gitlab group and all subgroups and projects inside..."
export GITLAB_GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
if [ $GITLAB_GROUP_ID ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID"
fi

echo -e "\n"
title_no_wait "Deleting Gitlab SSH key..."
export GITLAB_SSH_KEY_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/keys | jq --arg GITLAB_SSH_KEY "${GITLAB_SSH_KEY}" '.[] | select(.title==$GITLAB_SSH_KEY)' | jq -r '.id')
if [ $GITLAB_SSH_KEY_ID ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/user/keys/$GITLAB_SSH_KEY_ID"
fi

echo -e "\n"
title_no_wait "Deleting KCC GCP service account as a billing user..."
gcloud beta billing accounts remove-iam-policy-binding ${BILLING_ACCOUNT} \
  --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
  --role=roles/billing.user


echo -e "\n"
title_no_wait "Unsetting KUBECONFIG..."
unset KUBECONFIG

echo -e "\n"
title_no_wait "Manually run the following commands to finish cleanup:"
echo -e "rm -rf ${WORKDIR}"
echo -e "unset INFRA_FOLDER_ID"
echo -e "unset MAIN_FOLDER_ID"
echo -e "unset KCC_FOLDER_ID"
echo -e "unset FOLDER_ID"
echo -e "unset WORKDIR"