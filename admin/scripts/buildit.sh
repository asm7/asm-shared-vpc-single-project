#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

# Create WORKDIR and vars
${SCRIPT_DIR}/build_pre.sh -bn ${BUILD_NUMBER}

# Create logsdir
mkdir -p ${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}/logs
LOGSDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}/logs

# Create Gitlab groups and repos
title_no_wait "Building Gitlab main group..."
${SCRIPT_DIR}/build_gitlab_main_group.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/gitlab_main_group.log
title_no_wait "Building Gitlab subgroups and repos..."
nohup ${SCRIPT_DIR}/build_gitlab_groups_and_repos.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/gitlab_create_groups_and_repos.log 2>&1 &

# Create KCC resources
title_no_wait "Building main folder, kcc folder and kcc project..."
${SCRIPT_DIR}/build_create_main_kcc_folder_project.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/create_main_kcc_folder_project.log 2>&1

title_no_wait "Configuring KCC and Gitlab GCP SA IAM accounts and permissions..."
${SCRIPT_DIR}/build_kcc_proj_iam.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/kcc_proj_iam.log 2>&1 &

title_no_wait "Creating Gitlab builder image..."
${SCRIPT_DIR}/build_builder_image.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/builder_image.log 2>&1 &

title_no_wait "Creating kcc cluster..."
${SCRIPT_DIR}/build_kcc_cluster.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/create_kcc_cluster.log 2>&1

title_no_wait "Configuring kcc cluster..."
${SCRIPT_DIR}/build_configure_kcc_cluster.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/configure_kcc_cluster.log 2>&1

title_no_wait "Configuring Gitlab runner on the kcc cluster..."
${SCRIPT_DIR}/build_configure_gitlab_runner.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/configure_gitlab_runner.log 2>&1

title_no_wait "Configuring Gitlab CI variables..."
${SCRIPT_DIR}/build_gitlab_ci_variables.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/gitlab_ci_variables.log 2>&1
title_no_wait "Confirming all jobs have finished before starting the infrastructure pipeline..."
JOBS_CHECK=$(jobs)
while [ ${JOBS} ]; do
    sleep 5
done
title_no_wait "All jobs successfully finished."
title_no_wait "Starting infrastructure build pipeline..."
${SCRIPT_DIR}/build_start_infra_pipeline.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/start_infra_pipeline.log 2>&1

title_no_wait "You can view the infrastructure build pipeline through the link below:"
echo -e "https://gitlab.com/gcpworkshops-ameerab-${BUILD_NUMBER}-asm/platform-admins-group/infrastructure/-/pipelines"

title_no_wait "You can view the infrastructure build pipeline progress in shell through the following script:"
echo -e "${SCRIPT_DIR}/build_inspect_infra_pipeline.sh -bn ${BUILD_NUMBER}"
${SCRIPT_DIR}/build_inspect_infra_pipeline.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/inspect_infra_pipeline.log 2>&1

title_no_wait "Getting GKE cluster credentials..."
${SCRIPT_DIR}/build_get_cluster_credentials.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/get_cluster_credentials.log 2>&1

title_no_wait "Initializing the shared-cd repo..."
${SCRIPT_DIR}/repos_build_shared_cd.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/repos_build_shared_cd.log 2>&1

title_no_wait "Initializing the config (ACM) repo..."
${SCRIPT_DIR}/repos_build_config.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/repos_build_config.log 2>&1

title_no_wait "Deploying CockroachDB (for the Bank of Anthos app)..."
${SCRIPT_DIR}/db_build_cockroachdb.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/db_build_cockroachdb.log 2>&1

title_no_wait "You can view the CockroachDB build pipeline through the link below:"
echo -e "https://gitlab.com/gcpworkshops-ameerab-${BUILD_NUMBER}-asm/databases-group/cockroachdb/-/pipelines"

title_no_wait "You can view the CockroachDB build pipeline progress in shell through the following script:"
echo -e "${SCRIPT_DIR}/get_db_pipeline_status.sh -bn ${BUILD_NUMBER}"

echo -e "\e[95mSleeping for 60 seconds in between CockroachDB and RedisDB pipelines...\e[0m"
sleep 60

title_no_wait "Deploying RedisDB (for the Online Boutique app)..."
${SCRIPT_DIR}/db_build_redisdb.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/db_build_redisdb.log 2>&1

title_no_wait "You can view the RedisDB build pipeline through the link below:"
echo -e "https://gitlab.com/gcpworkshops-ameerab-${BUILD_NUMBER}-asm/databases-group/redis/-/pipelines"

title_no_wait "You can view the RedisDB build pipeline progress in shell through the following script:"
echo -e "${SCRIPT_DIR}/get_db_pipeline_status.sh -bn ${BUILD_NUMBER}"

title_no_wait "Deploying Online Boutique application pipelines..."
${SCRIPT_DIR}/shop_build.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/shop_build.log 2>&1

echo -e "\e[95mSleeping for 60 seconds in between Online Boutique and Bank of Anthos application pipelines to avoid hitting Cloud Build API rate limit...\e[0m"
sleep 60

title_no_wait "Deploying Bank of Anthos application pipelines..."
${SCRIPT_DIR}/bank_build.sh -bn ${BUILD_NUMBER} > ${LOGSDIR}/bank_build.log 2>&1

