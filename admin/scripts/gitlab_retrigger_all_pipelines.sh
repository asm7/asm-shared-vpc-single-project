#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\e[93mRetriggering Infrastructure pipelines...\e[0m"
${SCRIPT_DIR}/gitlab_infra_retrigger_pipeline.sh

echo -e "\e[93mRetriggering Database pipelines...\e[0m"
${SCRIPT_DIR}/gitlab_db_retrigger_pipelines.sh

echo -e "\e[93mRetriggering Online Boutique pipelines...\e[0m"
${SCRIPT_DIR}/gitlab_shop_retrigger_pipelines.sh

echo -e "\e[93mRetriggering Bank of Anthos pipelines...\e[0m"
${SCRIPT_DIR}/gitlab_shop_retrigger_pipelines.sh


