#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\n"
title_no_wait "*** BUILD INFRASTRUCTURE ***"
title_no_wait "Your BUILD_NUMBER is ${BUILD_NUMBER}."
echo -e "\n"

echo -e "\n"
title_no_wait "Creating a WORKDIR folder..."
mkdir -p ${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}

if [ ! $ADMIN_USER ]; then echo -e "\e[91mADMIN_USER not defined"; exit 1; fi
if [ ! $ORG_NAME ]; then echo -e "\e[91mORG_NAME not defined"; exit 1; fi
if [ ! $BILLING_ACCOUNT ]; then echo -e "\e[91mBILLING_ACCOUNT not defined"; exit 1; fi
if [ ! $ORG_ID ]; then echo -e "\e[91mADMIN_USER not defined"; exit 1; fi
if [ ! $GITLAB_TOKEN ]; then echo -e "\e[91mGITLAB_TOKEN not defined"; exit 1; fi

echo -e "\n"
title_no_wait "Creating vars..."
export INFRA_BRANCH_NAME="main"
export ORG_SHORT_NAME=${ORG_NAME%.*}
export USER_NAME=${ADMIN_USER%@*}
export USER_NAME=${USER_NAME:0:7}
export SUBNET_1_REGION=us-west2
export SUBNET_2_REGION=us-central1
export BUILD_NUMBER=${BUILD_NUMBER}
cat <<EOF >> $WORKDIR/vars.sh
# Copy and paste the remainder of the variables
# Org
# export BRANCH_NAME=main
export WORKDIR=$WORKDIR
export ADMIN_USER=$ADMIN_USER
export ORG_NAME=$ORG_NAME
export BILLING_ACCOUNT=$BILLING_ACCOUNT
export ORG_ID=$ORG_ID
export ORG_SHORT_NAME=${ORG_SHORT_NAME}
export USER_NAME=${USER_NAME}
export BUILD_NUMBER=${BUILD_NUMBER}
export MAIN_FOLDER_NAME=${USER_NAME}-${BUILD_NUMBER}-asm-shared-single
# KCC
export KCC_FOLDER_NAME=kcc
export KCC_PROJECT=proj-infra-admin
export KCC_GKE=kcc
export KCC_GKE_LOCATION=us-central1
export KCC_SERVICE_ACCOUNT=kcc-sa
# GCP Infra
export FOLDER_NAME=infra
export HOST_NET_PROJECT=proj-0-net-prod
export SVC_1_PROJECT=proj-1-ops-prod
export SVC_2_PROJECT=proj-2-bank-prod
export SVC_3_PROJECT=proj-3-shop-prod
# Gitlab groups and projects
export GITLAB_CI_GCP_SA_NAME=gitlab-ci-sa
export GITLAB_GROUP_NAME=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm
export GITLAB_ADMIN_SUBGROUP_NAME=platform-admins-group
export GITLAB_BANK_SUBGROUP_NAME=bank-of-anthos-group
export GITLAB_SHOP_SUBGROUP_NAME=online-boutique-group
export GITLAB_DATABASES_SUBGROUP_NAME=databases-group
export GITLAB_INFRA_PROJECT_NAME=infrastructure
export GITLAB_CONFIG_PROJECT_NAME=config
export GITLAB_CD_PROJECT_NAME=shared-cd
## Bank of Anthos repos
export GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME=accounts-db
export GITLAB_BANK_LEDGER_DB_PROJECT_NAME=ledger-db
export GITLAB_CONTACTS_PROJECT_NAME=contacts
export GITLAB_BANKFRONTEND_PROJECT_NAME=bankfrontend
export GITLAB_BALANCEREADER_PROJECT_NAME=balancereader
export GITLAB_LEDGERWRITER_PROJECT_NAME=ledgerwriter
export GITLAB_BANKLOADGENERATOR_PROJECT_NAME=bankloadgenerator
export GITLAB_TRANSACTIONHISTORY_PROJECT_NAME=transactionhistory
export GITLAB_USERSERVICE_PROJECT_NAME=userservice
## Online Boutique repos
export GITLAB_AD_PROJECT_NAME=ad
export GITLAB_CART_PROJECT_NAME=cart
export GITLAB_CHECKOUT_PROJECT_NAME=checkout
export GITLAB_CURRENCY_PROJECT_NAME=currency
export GITLAB_EMAIL_PROJECT_NAME=email
export GITLAB_SHOPFRONTEND_PROJECT_NAME=shopfrontend
export GITLAB_SHOPLOADGENERATOR_PROJECT_NAME=shoploadgenerator
export GITLAB_PAYMENT_PROJECT_NAME=payment
export GITLAB_PRODUCTCATALOG_PROJECT_NAME=productcatalog
export GITLAB_RECOMMENDATION_PROJECT_NAME=recommendation
export GITLAB_SHIPPING_PROJECT_NAME=shipping
export GITLAB_SHOP_REDIS_PROJECT_NAME=shopredis
export GITLAB_TEST_PROJECT_NAME=test
## Single repos
export GITLAB_BANK_PROJECT_NAME=bank-of-anthos
export GITLAB_SHOP_PROJECT_NAME=online-boutique
export GITLAB_COCKROACHDB_PROJECT_NAME=cockroachdb
export GITLAB_REDIS_PROJECT_NAME=redis
export GITLAB_SSH_KEY=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm-shared-single-sshkey
# GKE clusters and zones
export GKE1=gke-1-r1a-prod
export GKE2=gke-2-r1b-prod
export GKE3=gke-3-r2a-prod
export GKE4=gke-4-r2b-prod
export GKE5_INGRESS=ingress-config
export GKE1_ZONE=${SUBNET_1_REGION}-a
export GKE2_ZONE=${SUBNET_1_REGION}-b
export GKE3_ZONE=${SUBNET_2_REGION}-a
export GKE4_ZONE=${SUBNET_2_REGION}-b
export GKE5_INGRESS_ZONE=${SUBNET_1_REGION}-a
export KUBECONFIG=${WORKDIR}/asm-kubeconfig
# ASM
export ASM_VERSION=1.10.4-asm.6
export ASM_LABEL=asm-managed
export ASM_CHANNEL=regular
# ACM
export ACM_VERSION=1.6.1
# My IP
export TERMINAL_IP=$(curl -s ifconfig.me)
EOF

source $WORKDIR/vars.sh
gcloud config unset project

echo -e "\n"
title_no_wait "Cloning the repo inside WORKDIR..."
if [ ! -d "$WORKDIR/asm-shared-vpc-single-project-in-workdir" ]; then
    git clone https://gitlab.com/asm7/asm-shared-vpc-single-project.git -b ${INFRA_BRANCH_NAME} $WORKDIR/asm-shared-vpc-single-project-in-workdir
else
    rm -rf $WORKDIR/asm-shared-vpc-single-project-in-workdir
    git clone https://gitlab.com/asm7/asm-shared-vpc-single-project.git -b ${INFRA_BRANCH_NAME} $WORKDIR/asm-shared-vpc-single-project-in-workdir
fi
