#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Creating KCC GKE cluster..."
export KCC_GKE_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} container clusters list)
if [ ! "$KCC_GKE_EXISTS" ]; then
    DTSTART=$(date  --date="yesterday" +"%m-%d-%yT08:00:00Z")
    DTEND=$(date  --date="yesterday" +"%m-%d-%yT12:00:00Z")
    gcloud container clusters create ${KCC_GKE} \
    --project=${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --region=${KCC_GKE_LOCATION} \
    --node-locations=${KCC_GKE_LOCATION}-a,${KCC_GKE_LOCATION}-b \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "3" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --release-channel None \
    --no-enable-autoupgrade \
    --addons ConfigConnector \
    --maintenance-window-start ${DTSTART} \
    --maintenance-window-end ${DTEND} \
    --maintenance-window-recurrence FREQ=DAILY \
    --workload-pool=${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog --async
fi
