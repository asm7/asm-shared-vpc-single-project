#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

# Online Boutique application repo
shop_repos=(
    $GITLAB_AD_PROJECT_NAME
    $GITLAB_CART_PROJECT_NAME
    $GITLAB_CHECKOUT_PROJECT_NAME
    $GITLAB_CURRENCY_PROJECT_NAME
    $GITLAB_EMAIL_PROJECT_NAME
    $GITLAB_SHOPFRONTEND_PROJECT_NAME
    $GITLAB_SHOPLOADGENERATOR_PROJECT_NAME
    $GITLAB_PAYMENT_PROJECT_NAME
    $GITLAB_PRODUCTCATALOG_PROJECT_NAME
    $GITLAB_RECOMMENDATION_PROJECT_NAME
    $GITLAB_SHIPPING_PROJECT_NAME
    $GITLAB_SHOP_REDIS_PROJECT_NAME
    $GITLAB_TEST_PROJECT_NAME
)
for repo in "${shop_repos[@]}"
do
    echo -e "\e[95mBuilding $repo repo...\e[0m"
    [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_SHOP_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
    cd ${WORKDIR}/repos/${repo} && git pull
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/online-boutique/${repo}/. ${WORKDIR}/repos/${repo}/.
    cd ${WORKDIR}/repos/${repo}
    envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
    rm -rf .gitlab-ci.yml_tmpl
    git add .
    git commit -m "initial commit"
    git push || true
    echo -e "\e[95mSleeping for 15 seconds in between builds to avoid hitting Cloud Build API rate limit...\e[0m"
    sleep 15
done

echo -e "\e[95mAccess all Online Boutique pipelines through the following links:\e[0m"
# Ad
echo -e "${GITLAB_AD_PROJECT_WEB_URL}/-/pipelines"
# Cart
echo -e "${GITLAB_CART_PROJECT_WEB_URL}/-/pipelines"
# Checkout
echo -e "${GITLAB_CHECKOUT_PROJECT_WEB_URL}/-/pipelines"
# Currency
echo -e "${GITLAB_CURRENCY_PROJECT_WEB_URL}/-/pipelines"
# Email
echo -e "${GITLAB_EMAIL_PROJECT_WEB_URL}/-/pipelines"
# Payment
echo -e "${GITLAB_PAYMENT_PROJECT_WEB_URL}/-/pipelines"
# Product catalog
echo -e "${GITLAB_PRODUCTCATALOG_PROJECT_WEB_URL}/-/pipelines"
# Recommendation
echo -e "${GITLAB_RECOMMENDATION_PROJECT_WEB_URL}/-/pipelines"
# Shipping
echo -e "${GITLAB_SHIPPING_PROJECT_WEB_URL}/-/pipelines"
# Shopfrontend
echo -e "${GITLAB_SHOPFRONTEND_PROJECT_WEB_URL}/-/pipelines"
# Shoploadgenerator
echo -e "${GITLAB_SHOPLOADGENERATOR_PROJECT_WEB_URL}/-/pipelines"
# Redis DB - This is not used in this setup. Insetad a Redis cluster is used. However, this is a simple Deployment and can be used as a standalone single node Redis.
echo -e "${GITLAB_SHOP_REDIS_PROJECT_WEB_URL}/-/pipelines"

echo -e "\e[95mAccess Online Boutique application through the following link:\e[0m"
echo -e "https://shop.endpoints.${SVC_1_PROJECT}-${FOLDER_ID}.cloud.goog"


