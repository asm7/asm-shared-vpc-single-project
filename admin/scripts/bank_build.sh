#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

# Bank of Anthos application repo
bank_repos=(
    $GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME
    $GITLAB_BANK_LEDGER_DB_PROJECT_NAME
    $GITLAB_CONTACTS_PROJECT_NAME
    $GITLAB_BANKFRONTEND_PROJECT_NAME
    $GITLAB_BALANCEREADER_PROJECT_NAME
    $GITLAB_LEDGERWRITER_PROJECT_NAME
    $GITLAB_BANKLOADGENERATOR_PROJECT_NAME
    $GITLAB_TRANSACTIONHISTORY_PROJECT_NAME
    $GITLAB_USERSERVICE_PROJECT_NAME
)
for repo in "${bank_repos[@]}"
do
    echo -e "\e[95mBuilding $repo repo...\e[0m"
    [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_BANK_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
    cd ${WORKDIR}/repos/${repo} && git pull
    cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/bank-of-anthos/${repo}/. ${WORKDIR}/repos/${repo}/.
    cd ${WORKDIR}/repos/${repo}
    envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
    rm -rf .gitlab-ci.yml_tmpl
    git add .
    git commit -m "initial commit"
    git push || true
    echo -e "\e[95mSleeping for 15 seconds in between builds to avoid hitting Cloud Build API rate limit...\e[0m"
    sleep 15
done

echo -e "\e[95mAccess all Bank of Anthos pipelines through the following links:\e[0m"
# Balancereader
echo -e "${GITLAB_BALANCEREADER_PROJECT_WEB_URL}/-/pipelines"
# Contacts
echo -e "${GITLAB_CONTACTS_PROJECT_WEB_URL}/-/pipelines"
# Bankfrontend
echo -e "${GITLAB_BANKFRONTEND_PROJECT_WEB_URL}/-/pipelines"
# Ledgerwriter
echo -e "${GITLAB_LEDGERWRITER_PROJECT_WEB_URL}/-/pipelines"
# Transactionhistory
echo -e "${GITLAB_TRANSACTIONHISTORY_PROJECT_WEB_URL}/-/pipelines"
# Userservice
echo -e "${GITLAB_USERSERVICE_PROJECT_WEB_URL}/-/pipelines"
# Bankloadgenerator
echo -e "${GITLAB_BANKLOADGENERATOR_PROJECT_WEB_URL}/-/pipelines"
# Accounts DB - This is not used in this setup. insetad CockroachDB is used. However, it is deployed and can be used.
echo -e "${GITLAB_BANK_ACCOUNTS_DB_PROJECT_WEB_URL}/-/pipelines"
# Ledger DB - This is not used in this setup. insetad CockroachDB is used. However, it is deployed and can be used.
echo -e "${GITLAB_BANK_LEDGER_DB_PROJECT_WEB_URL}/-/pipelines"

echo -e "\e[95mAccess Bank of Anthos application through the following link:\e[0m"
echo -e "https://bank.endpoints.${SVC_1_PROJECT}-${FOLDER_ID}.cloud.goog"
