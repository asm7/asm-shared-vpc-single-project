#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--repo | -r Repo name."
   echo -e "\t--group | -g Gitlab group name"
   echo -e "\t--subgroup | -sg Gitlab subgroup name"
   echo -e "\t--token | -t Gitlab token."
   echo -e "\t--workdir | -w Workdir path"
   exit 1 # Exit script after printing help
}

# Setting default value
REPO_NAME=
REPO_ID=
REPO_SSH_URL=
REPO_WEB_URL=
GROUP_NAME=
GROUP_ID=
SUBGROUP_NAME=
SUBGROUP_ID=
VAR_PREFIX=
TOKEN=${GITLAB_TOKEN}
WORKDIR_PATH=${WORKDIR}

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --repo | -r )                 shift
                                      REPO_NAME=$1
                                      ;;
        --group | -g )                shift
                                      GROUP_NAME=$1
                                      ;;
        --subgroup| -sg )             shift
                                      SUBGROUP_NAME=$1
                                      ;;
        --token | -t )                shift
                                      TOKEN=$1
                                      ;;
        --workdir | -w )              shift
                                      WORKDIR_PATH=$1
                                      ;;
        --varprefix | -v )            shift
                                      VAR_PREFIX=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

# Get User ID
USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" https://gitlab.com/api/v4/user | jq -r '.id')

# Check if group exists otherwise create it
if [ $GROUP_NAME ]; then
    GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GROUP_NAME "$GROUP_NAME" '.[] | select(.name==$GROUP_NAME)' | jq -r '.id')
    if [ ! "${GROUP_ID}" ]; then
        echo -e "\e[95mCreating group $GROUP_NAME ...\e[0m"
        curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -X POST "https://gitlab.com/api/v4/groups?name=${GROUP_NAME}&path=${GROUP_NAME}" > /dev/null
        GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GROUP_NAME "$GROUP_NAME" '.[] | select(.name==$GROUP_NAME)' | jq -r '.id')
    else
        echo -e "\e[92mGroup $GROUP_NAME already exists with ID $GROUP_ID.\e[0m"
    fi
fi

# Check if subgroup exists otherwise create it
if [ $SUBGROUP_NAME ]; then
    SUBGROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/groups/${GROUP_ID}/subgroups" | jq --arg SUBGROUP_NAME "$SUBGROUP_NAME"  '.[] | select(.name==$SUBGROUP_NAME)' | jq -r '.id')
    if [ ! "${SUBGROUP_ID}" ]; then
        if [ "${GROUP_ID}" ]; then
            echo -e "\e[95mCreating subgroup $SUBGROUP_NAME in group $GROUP_NAME...\e[0m"
            curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -X POST "https://gitlab.com/api/v4/groups?name=${SUBGROUP_NAME}&path=${SUBGROUP_NAME}&parent_id=${GROUP_ID}" > /dev/null
            SUBGROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/groups/${GROUP_ID}/subgroups" | jq --arg SUBGROUP_NAME "$SUBGROUP_NAME"  '.[] | select(.name==$SUBGROUP_NAME)' | jq -r '.id')
            echo -e "\e[92mSubgroup $SUBGROUP_NAME created in group $GROUP_NAME with ID $SUBGROUP_ID\e[0m"
        else
            echo -e "\e[91mGroup name is required to create a subgroup.\e[0m"
            exit 1
        fi
    else
        echo -e "\e[92mSubgroup $SUBGROUP_NAME already exists with ID $SUBGROUP_ID."
    fi
fi

# Create repo
if [ "${REPO_NAME}" ]; then
    if [ "${GROUP_ID}" ]; then
        if [ "${SUBGROUP_ID}" ]; then
            REPO_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${GROUP_NAME}%2F$SUBGROUP_NAME%2F$REPO_NAME" | jq -r '.id')
            if [[ ${REPO_ID} -eq "null" ]]; then
                echo -e "\e[95mCreating repo $REPO_NAME in subgroup $SUBGROUP_NAME...\e[0m"
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${REPO_NAME}&namespace_id=${SUBGROUP_ID}&shared_runners_enabled=false" > /dev/null
                REPO_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${GROUP_NAME}%2F$SUBGROUP_NAME%2F$REPO_NAME" | jq -r '.id')
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X POST "https://gitlab.com/api/v4/projects/${REPO_ID}/repository/branches?branch=main&ref=master" > /dev/null
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X PUT "https://gitlab.com/api/v4/projects/${REPO_ID}?default_branch=main" > /dev/null
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X DELETE "https://gitlab.com/api/v4/projects/${REPO_ID}/repository/branches/master" > /dev/null
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X POST "https://gitlab.com/api/v4/projects/${REPO_ID}/protected_branches?name=main" > /dev/null
                REPO_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.ssh_url_to_repo')
                REPO_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.web_url')
                [ -f "${WORKDIR_PATH}/tmp/${GITLAB_SSH_KEY}-acm-key.pub" ] && curl -s --request POST --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}/deploy_keys/" --form "title=acm-key" --form "key=$(cat ${WORKDIR_PATH}/tmp/${GITLAB_SSH_KEY}-acm-key.pub)" --form "can_push=true" > /dev/null
            else
                echo -e "\e[95mRepo $REPO_NAME exists in subgroup $SUBGROUP_NAME with ID $REPO_ID\e[0m"
                REPO_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.ssh_url_to_repo')
                REPO_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.web_url')
            fi
        else
            REPO_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${GROUP_NAME}%2F$REPO_NAME" | jq -r '.id')
            if [[ ${REPO_ID} -eq "null" ]]; then
                echo -e "\e[95mCreating repo $REPO_NAME in group $GROUP_NAME...\e[0m"
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${REPO_NAME}&namespace_id=${GROUP_ID}&shared_runners_enabled=false" > /dev/null
                REPO_ID=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${GROUP_NAME}%2F$REPO_NAME" | jq -r '.id')
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X POST "https://gitlab.com/api/v4/projects/${REPO_ID}/repository/branches?branch=main&ref=master" > /dev/null
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X PUT "https://gitlab.com/api/v4/projects/${REPO_ID}?default_branch=main" > /dev/null
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X DELETE "https://gitlab.com/api/v4/projects/${REPO_ID}/repository/branches/master" > /dev/null
                curl -s --header "PRIVATE-TOKEN: ${TOKEN}" -H "Content-Type: application/json" -X POST "https://gitlab.com/api/v4/projects/${REPO_ID}/protected_branches?name=main" > /dev/null
                REPO_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.ssh_url_to_repo')
                REPO_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.web_url')
                [ -f "${WORKDIR_PATH}/tmp/${GITLAB_SSH_KEY}-acm-key.pub" ] && curl -s --request POST --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}/deploy_keys/" --form "title=acm-key" --form "key=$(cat ${WORKDIR_PATH}/tmp/${GITLAB_SSH_KEY}-acm-key.pub)" --form "can_push=true" > /dev/null
            else
                echo -e "\e[95mRepo $REPO_NAME exists in group $GROUP_NAME with ID $REPO_ID\e[0m"
                REPO_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.ssh_url_to_repo')
                REPO_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.web_url')
            fi
        fi
    else
        # If both group or subgroup does not exist
        echo -e "\e[91mGroup or subgroup must be provided to create a repo.\e[0m"
        exit 1
    fi
else
    echo -e "\e[95mGetting Repo SSH and Web URL links...\e[0m"
    # If repo exists, get SSH and Web URL
    REPO_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.ssh_url_to_repo')
    REPO_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}" | jq -r '.web_url')
fi

# Adding to vars
[ $USER_ID ] &&  echo -e "export GITLAB_USER_ID=$USER_ID" >> $WORKDIR/vars.sh
[ $GROUP_ID ] &&  echo -e "export GITLAB_${VAR_PREFIX}_GROUP_ID=$GROUP_ID" >> $WORKDIR/vars.sh
[ $SUBGROUP_ID ] &&  echo -e "export GITLAB_${VAR_PREFIX}_SUBGROUP_ID=$SUBGROUP_ID" >> $WORKDIR/vars.sh
[ $REPO_ID ] &&  echo -e "export GITLAB_${VAR_PREFIX}_PROJECT_ID=$REPO_ID" >> $WORKDIR/vars.sh
[ $REPO_SSH_URL ] && echo -e "export GITLAB_${VAR_PREFIX}_PROJECT_SSH_URL=$REPO_SSH_URL" >> $WORKDIR/vars.sh
[ $REPO_WEB_URL ] && echo -e "export GITLAB_${VAR_PREFIX}_PROJECT_WEB_URL=$REPO_WEB_URL" >> $WORKDIR/vars.sh

echo -e "\n"
echo -e "\e[92mRepo Name: $REPO_NAME\e[0m"
echo -e "\e[92mUser ID: $USER_ID\e[0m"
echo -e "\e[92mGroup ID: $GROUP_ID\e[0m"
echo -e "\e[92mSubgroup ID: $SUBGROUP_ID\e[0m"
echo -e "\e[92mRepo ID: $REPO_ID\e[0m"
echo -e "\e[92mRepo SSH URL: $REPO_SSH_URL\e[0m"
echo -e "\e[92mRepo Web URL: $REPO_WEB_URL\e[0m"
echo -e "\n"
