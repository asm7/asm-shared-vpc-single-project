#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\e[93mGetting Infrastructure pipeline status...\e[0m"
${SCRIPT_DIR}/get_infra_pipeline_status.sh

echo -e "\e[93mGetting Database pipeline status...\e[0m"
${SCRIPT_DIR}/get_db_pipeline_status.sh

echo -e "\e[93mGetting Online Boutique pipeline status...\e[0m"
${SCRIPT_DIR}/get_bank_pipeline_status.sh

echo -e "\e[93mGetting Bank of Anthos pipeline status...\e[0m"
${SCRIPT_DIR}/get_shop_pipeline_status.sh


