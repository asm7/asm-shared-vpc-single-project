#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Getting cluster credentials to all clusters..."
export INFRA_FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
[ -z "$INFRA_FOLDER_ID" ] && echo "INFRA_FOLDER_ID is not exported" || echo "INFRA_FOLDER_ID is $INFRA_FOLDER_ID"
echo -e "export INFRA_FOLDER_ID=$INFRA_FOLDER_ID" >> $WORKDIR/vars.sh
echo -e "export FOLDER_ID=$INFRA_FOLDER_ID" >> $WORKDIR/vars.sh
gcloud config set project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=INFRA_FOLDER_ID" --form "value=${INFRA_FOLDER_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=FOLDER_ID" --form "value=${INFRA_FOLDER_ID}" --form "protected=true"

gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE5_INGRESS} --zone ${GKE5_INGRESS_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}

kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE1_ZONE}_${GKE1}
kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE2_ZONE}_${GKE2}
kubectl ctx ${GKE3}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE3_ZONE}_${GKE3}
kubectl ctx ${GKE4}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE4_ZONE}_${GKE4}
kubectl ctx ${GKE5_INGRESS}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE5_INGRESS_ZONE}_${GKE5_INGRESS}
