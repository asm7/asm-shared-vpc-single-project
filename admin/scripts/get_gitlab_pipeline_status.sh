#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--repo-id | -r Gitlab repo ID."
   echo -e "\t--gitlab-token | -gt Gitlab private token. Default is set to variable value GITLAB_TOKEN"
   echo -e "\tExample usage:"
   echo -e "\t./get_gitlab_pipeline_status.sh -r GITLAB_REPO_ID -gt YOUR_GITLAB_TOKEN"
   exit 1 # Exit script after printing help
}

# Setting default value
REPO_ID=
TOKEN=${GITLAB_TOKEN}

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --repo-id | -r )              shift
                                      REPO_ID=$1
                                      ;;
        --gitlab-token | -gt )        shift
                                      TOKEN=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done


# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

if [[ ! ${REPO_ID} ]]; then
    title_no_wait "Please provide a REPO_ID."
    title_no_wait "Please run './get_gitlab_pipeline_status.sh --help'."
    exit 1
fi

if [[ ! ${TOKEN} ]]; then
    title_no_wait "GITLAB_TOKEN is not set. Please set the variable GITLAB_TOKEN and try again."
    title_no_wait "Please run './get_gitlab_pipeline_status.sh --help'."
    exit 1
fi

STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${REPO_ID}/pipelines" | jq -r '.[0].status')
echo $STATUS
