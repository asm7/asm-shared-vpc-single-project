#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Creating the main folder..."
export MAIN_FOLDER_EXISTS=$(gcloud alpha resource-manager folders list --organization=$ORG_ID | grep ${MAIN_FOLDER_NAME})
if [ ! "$MAIN_FOLDER_EXISTS" ]; then
    MAIN_FOLDER_ID=$(gcloud alpha resource-manager folders create \
    --display-name=${MAIN_FOLDER_NAME} \
    --organization=${ORG_ID} \
    --format='value(name)')
    MAIN_FOLDER_ID=${MAIN_FOLDER_ID##*/}
    [ -z "$MAIN_FOLDER_ID" ] && echo "MAIN_FOLDER_ID is not exported" && exit 1 || echo "Main Folder ID is $MAIN_FOLDER_ID"
    echo -e "export MAIN_FOLDER_ID=$MAIN_FOLDER_ID" >> $WORKDIR/vars.sh
fi

echo -e "\n"
title_no_wait "Creating KCC folder..."
export KCC_FOLDER_EXISTS=$(gcloud resource-manager folders list --folder=${MAIN_FOLDER_ID} | grep ${KCC_FOLDER_NAME})
if [ ! "$KCC_FOLDER_EXISTS" ]; then
    KCC_FOLDER_ID=$(gcloud alpha resource-manager folders create \
    --display-name=${KCC_FOLDER_NAME} \
    --folder=${MAIN_FOLDER_ID} \
    --format='value(name)')
    KCC_FOLDER_ID=${KCC_FOLDER_ID##*/}
    [ -z "$KCC_FOLDER_ID" ] && echo "KCC_FOLDER_ID is not exported" && exit 1 || echo "KCC Folder ID is $KCC_FOLDER_ID"
    echo -e "export KCC_FOLDER_ID=$KCC_FOLDER_ID" >> $WORKDIR/vars.sh
fi

echo -e "\n"
title_no_wait "Creating KCC GCP Project..."
if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
    echo "KCC project already exists."
else
    gcloud projects create ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --folder ${KCC_FOLDER_ID}
    gcloud beta billing projects link ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}
    gcloud services enable \
    --project ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    container.googleapis.com \
    cloudbilling.googleapis.com \
    cloudbuild.googleapis.com \
    sqladmin.googleapis.com \
    servicenetworking.googleapis.com \
    accesscontextmanager.googleapis.com \
    cloudresourcemanager.googleapis.com
fi
