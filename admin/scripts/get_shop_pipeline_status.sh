#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

TOKEN=${GITLAB_TOKEN}
for REPO in ${GITLAB_SHOPFRONTEND_PROJECT_ID} \
             ${GITLAB_SHOPLOADGENERATOR_PROJECT_ID} \
             ${GITLAB_PAYMENT_PROJECT_ID} \
             ${GITLAB_PRODUCTCATALOG_PROJECT_ID} \
             ${GITLAB_RECOMMENDATION_PROJECT_ID} \
             ${GITLAB_SHIPPING_PROJECT_ID} \
             ${GITLAB_SHOP_REDIS_PROJECT_ID} \
             ${GITLAB_AD_PROJECT_ID} \
             ${GITLAB_CART_PROJECT_ID} \
             ${GITLAB_CHECKOUT_PROJECT_ID} \
             ${GITLAB_CURRENCY_PROJECT_ID} \
             ${GITLAB_EMAIL_PROJECT_ID}; do
    REPO_NAME=$(curl -s --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${REPO}" | jq -r '.name')
    STATUS=$(${SCRIPT_DIR}/get_gitlab_pipeline_status.sh -r ${REPO})
    URL=$(${SCRIPT_DIR}/get_gitlab_pipeline_url.sh -r ${REPO})
    title_no_wait "${REPO_NAME} pipeline status is ${STATUS}"
    title_no_wait "${REPO_NAME} pipeline URL is ${URL}"
done

