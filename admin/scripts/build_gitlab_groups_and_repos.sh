#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Creating Gitlab subgroups and projects for admin, apps and databases..."
export CREATE_REPO_SCRIPT="${WORKDIR}/asm-shared-vpc-single-project-in-workdir/admin/scripts/create_gitlab_repo.sh"
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_INFRA_PROJECT_NAME} --varprefix INFRA
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_CONFIG_PROJECT_NAME} --varprefix CONFIG
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_CD_PROJECT_NAME} --varprefix CD
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME} --varprefix BANK_ACCOUNTS_DB
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANK_LEDGER_DB_PROJECT_NAME} --varprefix BANK_LEDGER_DB
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_CONTACTS_PROJECT_NAME} --varprefix CONTACTS
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANKFRONTEND_PROJECT_NAME} --varprefix BANKFRONTEND
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BALANCEREADER_PROJECT_NAME} --varprefix BALANCEREADER
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_LEDGERWRITER_PROJECT_NAME} --varprefix LEDGERWRITER
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANKLOADGENERATOR_PROJECT_NAME} --varprefix BANKLOADGENERATOR
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_TRANSACTIONHISTORY_PROJECT_NAME} --varprefix TRANSACTIONHISTORY
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_USERSERVICE_PROJECT_NAME} --varprefix USERSERVICE
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_AD_PROJECT_NAME} --varprefix AD
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CART_PROJECT_NAME} --varprefix CART
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CHECKOUT_PROJECT_NAME} --varprefix CHECKOUT
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CURRENCY_PROJECT_NAME} --varprefix CURRENCY
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_EMAIL_PROJECT_NAME} --varprefix EMAIL
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOPFRONTEND_PROJECT_NAME} --varprefix SHOPFRONTEND
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOPLOADGENERATOR_PROJECT_NAME} --varprefix SHOPLOADGENERATOR
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_PAYMENT_PROJECT_NAME} --varprefix PAYMENT
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_PRODUCTCATALOG_PROJECT_NAME} --varprefix PRODUCTCATALOG
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_RECOMMENDATION_PROJECT_NAME} --varprefix RECOMMENDATION
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHIPPING_PROJECT_NAME} --varprefix SHIPPING
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOP_REDIS_PROJECT_NAME} --varprefix SHOP_REDIS
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_TEST_PROJECT_NAME} --varprefix SHOP_TEST
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_DATABASES_SUBGROUP_NAME} --repo ${GITLAB_COCKROACHDB_PROJECT_NAME} --varprefix COCKROACHDB
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_DATABASES_SUBGROUP_NAME} --repo ${GITLAB_REDIS_PROJECT_NAME} --varprefix REDIS
source ${WORKDIR}/vars.sh

