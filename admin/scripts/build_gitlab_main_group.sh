#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Create a Gitlab SSH key..."
if [ ! -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key ]; then
    mkdir -p ${WORKDIR}/tmp && cd ${WORKDIR}/tmp
    ssh-keygen -t rsa -b 4096 \
    -C "" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key
    cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key | base64 > ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key-base64.txt
    ssh-keygen -t rsa -b 4096 \
    -C "${ADMIN_USER}" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    eval `ssh-agent`
    ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    echo -e "eval \`ssh-agent\` && ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key" >> $WORKDIR/vars.sh
fi

echo -e "\n"
title_no_wait "Adding the Gitlab SSH key..."
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" --form "title=${GITLAB_SSH_KEY}" --form "key=$(cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key.pub)"

export GITLAB_USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user" | jq -r '.id')
[ -z "$GITLAB_USER_ID" ] && echo "GITLAB_USER_ID is not exported" || echo "GITLAB_USER_ID is $GITLAB_USER_ID"
echo -e "export GITLAB_USER_ID=$GITLAB_USER_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating the main Gitlab group..."
export GITLAB_MAIN_GROUP_EXISTS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
if [ -z "$GITLAB_MAIN_GROUP_EXISTS" ]; then
    curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --form "name=${GITLAB_GROUP_NAME}" --form "path=${GITLAB_GROUP_NAME}" \
    --form "file=@${WORKDIR}/asm-shared-vpc-single-project-in-workdir/admin/scripts/gitlab-parent-group.tar.gz" \
    "https://gitlab.com/api/v4/groups/import"
    # Get group ID
    export GITLAB_GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
    [ -z "$GITLAB_GROUP_ID" ] && echo "GITLAB_GROUP_ID is not exported" || echo "GITLAB_GROUP_ID is $GITLAB_GROUP_ID"
    echo -e "export GITLAB_GROUP_ID=$GITLAB_GROUP_ID" >> $WORKDIR/vars.sh
    else
    echo -e "Gitlab main group already exists with ID $GITLAB_GROUP_ID."
fi

