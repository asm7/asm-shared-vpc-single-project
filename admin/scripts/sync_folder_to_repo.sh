#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--folder | -f Folder path to sync repo with."
   echo -e "\t--reposshurl | -r Gitlab repo SSH url"
   echo -e "\t--reponame | -n Gitlab repo name"
   echo -e "\t--workdir | -w WORKDIR path"
   exit 1 # Exit script after printing help
}

# Setting default value
REPO_SSH_URL=
REPO_NAME=
FOLDER_PATH=
WORKDIR_PATH=${WORKDIR}

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --folder | -f )               shift
                                      FOLDER_PATH=$1
                                      ;;
        --reposshurl | -r )           shift
                                      REPO_SSH_URL=$1
                                      ;;
        --reponame | -n )             shift
                                      REPO_NAME=$1
                                      ;;
        --workdir | -w )              shift
                                      WORKDIR_PATH=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

mkdir -p ${WORKDIR_PATH}/repos
REPO_FOLDER=${WORKDIR_PATH}/repos/${REPO_NAME}

rm -rf ${REPO_FOLDER}
eval `ssh-agent` && ssh-add ${WORKDIR_PATH}/tmp/${GITLAB_SSH_KEY}-key
git clone ${REPO_SSH_URL} ${REPO_FOLDER}
cd ${REPO_FOLDER}
cp -r ${FOLDER_PATH}/. ${REPO_FOLDER}/.
git add .
git commit -am "commit"
git push

cd ${WORKDIR_PATH}
rm -rf ${REPO_FOLDER}