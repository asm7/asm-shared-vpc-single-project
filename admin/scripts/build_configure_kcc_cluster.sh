#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
echo -e "export KUBECONFIG=$WORKDIR/asm-kubeconfig" >> $WORKDIR/vars.sh

# Check if cluster exists first or wait for it
KCC_GKE_EXISTS=
while [ -z ${KCC_GKE_EXISTS} ]; do
    echo "Checking to see if the kcc cluster exists..."
    KCC_GKE_EXISTS=$(gcloud container clusters list --project ${KCC_PROJECT}-${KCC_FOLDER_ID} --format='value(name)')
    sleep 5
done

echo "kcc cluster exists."

# Wait until cluster is RUNNING
KCC_GKE_STATUS=PROVISIONING
until [[ ${KCC_GKE_STATUS} = "RUNNING" ]]; do
    echo "Waiting until kcc cluster is RUNNING..."
    KCC_GKE_STATUS=$(gcloud container clusters describe ${KCC_GKE} --project ${KCC_PROJECT}-${KCC_FOLDER_ID} --region ${KCC_GKE_LOCATION} --format='value(status)')
    sleep 10
done

echo "kcc cluster is RUNNING."

# Get cluster credentials
echo "Getting kcc cluster credentials..."
gcloud container clusters get-credentials ${KCC_GKE} --region ${KCC_GKE_LOCATION} --project ${KCC_PROJECT}-${KCC_FOLDER_ID}
kubectl ctx ${KCC_GKE}=gke_${KCC_PROJECT}-${KCC_FOLDER_ID}_${KCC_GKE_LOCATION}_${KCC_GKE}

echo -e "\n"
title_no_wait "Creating WorkloadIdentity for the KCC GCP SA..."
gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts add-iam-policy-binding \
${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
--member="serviceAccount:${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
--role="roles/iam.workloadIdentityUser"

echo -e "\n"
title_no_wait "Creating ConfigConnector resource in the KCC cluster..."
cat <<EOF > $WORKDIR/${KCC_GKE}-configconnector.yaml
apiVersion: core.cnrm.cloud.google.com/v1beta1
kind: ConfigConnector
metadata:
  # the name is restricted to ensure that there is only one
  # ConfigConnector instance installed in your cluster
  name: configconnector.core.cnrm.cloud.google.com
spec:
  mode: cluster
  googleServiceAccount: "${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com"
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/${KCC_GKE}-configconnector.yaml

echo -e "\n"
title_no_wait "Creating Organization namespace resource in the KCC cluster..."
cat <<EOF > $WORKDIR/${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ${ORG_SHORT_NAME}
  annotations:
    cnrm.cloud.google.com/organization-id: "${ORG_ID}"
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
