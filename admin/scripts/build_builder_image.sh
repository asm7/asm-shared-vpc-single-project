#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Creating the builder docker image in GCR..."
export BUILDER_IMAGE_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds list | grep builder)
if [ ! "$BUILDER_IMAGE_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds submit $WORKDIR/asm-shared-vpc-single-project-in-workdir/infrastructure/builder/. --tag=gcr.io/${KCC_PROJECT}-${KCC_FOLDER_ID}/builder
    gsutil defacl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -r -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    cd ${WORKDIR}
fi
