#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

usage()
{
   echo ""
   echo "Usage: $0"
   echo -e "\t--build_number | -bn Two digit build number, for example 01 or 34. This is so you can create more than one environment. Default BUILD_NUMBER is 01."
   echo -e "\tExample usage:"
   echo -e "\t./buildeverything.sh -bn 10"
   exit 1 # Exit script after printing help
}

# Setting default value
BUILD_NUMBER=01

# Define bash args
while [ "$1" != "" ]; do
    case $1 in
        --build_number | -bn )        shift
                                      BUILD_NUMBER=$1
                                      ;;
        --help | -h )                 usage
                                      exit
    esac
    shift
done

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\n"
title_no_wait "*** BUILD INFRASTRUCTURE ***"
title_no_wait "Your BUILD_NUMBER is ${BUILD_NUMBER}."
echo -e "\n"

echo -e "\n"
title_no_wait "Creating a WORKDIR folder..."
mkdir -p ${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}
export WORKDIR=${SCRIPT_DIR}/../../../asm-shared-vpc-single-proj-gitlab-${BUILD_NUMBER}

if [ ! $ADMIN_USER ]; then echo -e "\e[91mADMIN_USER not defined"; exit 1; fi
if [ ! $ORG_NAME ]; then echo -e "\e[91mORG_NAME not defined"; exit 1; fi
if [ ! $BILLING_ACCOUNT ]; then echo -e "\e[91mBILLING_ACCOUNT not defined"; exit 1; fi
if [ ! $ORG_ID ]; then echo -e "\e[91mADMIN_USER not defined"; exit 1; fi
if [ ! $GITLAB_TOKEN ]; then echo -e "\e[91mGITLAB_TOKEN not defined"; exit 1; fi

echo -e "\n"
title_no_wait "Creating vars..."
export INFRA_BRANCH_NAME="aa/mcp"
export ORG_SHORT_NAME=${ORG_NAME%.*}
export USER_NAME=${ADMIN_USER%@*}
export USER_NAME=${USER_NAME:0:7}
export SUBNET_1_REGION=us-west2
export SUBNET_2_REGION=us-central1
export BUILD_NUMBER=${BUILD_NUMBER}
cat <<EOF >> $WORKDIR/vars.sh
# Copy and paste the remainder of the variables
# Org
# export BRANCH_NAME=main
export WORKDIR=$WORKDIR
export ADMIN_USER=$ADMIN_USER
export ORG_NAME=$ORG_NAME
export BILLING_ACCOUNT=$BILLING_ACCOUNT
export ORG_ID=$ORG_ID
export ORG_SHORT_NAME=${ORG_SHORT_NAME}
export USER_NAME=${USER_NAME}
export BUILD_NUMBER=${BUILD_NUMBER}
export MAIN_FOLDER_NAME=${USER_NAME}-${BUILD_NUMBER}-asm-shared-single
# KCC
export KCC_FOLDER_NAME=kcc
export KCC_PROJECT=proj-infra-admin
export KCC_GKE=kcc
export KCC_GKE_LOCATION=us-central1
export KCC_SERVICE_ACCOUNT=kcc-sa
# GCP Infra
export FOLDER_NAME=infra
export HOST_NET_PROJECT=proj-0-net-prod
export SVC_1_PROJECT=proj-1-ops-prod
export SVC_2_PROJECT=proj-2-bank-prod
export SVC_3_PROJECT=proj-3-shop-prod
# Gitlab groups and projects
export GITLAB_CI_GCP_SA_NAME=gitlab-ci-sa
export GITLAB_GROUP_NAME=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm
export GITLAB_ADMIN_SUBGROUP_NAME=platform-admins-group
export GITLAB_BANK_SUBGROUP_NAME=bank-of-anthos-group
export GITLAB_SHOP_SUBGROUP_NAME=online-boutique-group
export GITLAB_DATABASES_SUBGROUP_NAME=databases-group
export GITLAB_INFRA_PROJECT_NAME=infrastructure
export GITLAB_CONFIG_PROJECT_NAME=config
export GITLAB_CD_PROJECT_NAME=shared-cd
## Bank of Anthos repos
export GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME=accounts-db
export GITLAB_BANK_LEDGER_DB_PROJECT_NAME=ledger-db
export GITLAB_CONTACTS_PROJECT_NAME=contacts
export GITLAB_BANKFRONTEND_PROJECT_NAME=bankfrontend
export GITLAB_BALANCEREADER_PROJECT_NAME=balancereader
export GITLAB_LEDGERWRITER_PROJECT_NAME=ledgerwriter
export GITLAB_BANKLOADGENERATOR_PROJECT_NAME=bankloadgenerator
export GITLAB_TRANSACTIONHISTORY_PROJECT_NAME=transactionhistory
export GITLAB_USERSERVICE_PROJECT_NAME=userservice
## Online Boutique repos
export GITLAB_AD_PROJECT_NAME=ad
export GITLAB_CART_PROJECT_NAME=cart
export GITLAB_CHECKOUT_PROJECT_NAME=checkout
export GITLAB_CURRENCY_PROJECT_NAME=currency
export GITLAB_EMAIL_PROJECT_NAME=email
export GITLAB_SHOPFRONTEND_PROJECT_NAME=shopfrontend
export GITLAB_SHOPLOADGENERATOR_PROJECT_NAME=shoploadgenerator
export GITLAB_PAYMENT_PROJECT_NAME=payment
export GITLAB_PRODUCTCATALOG_PROJECT_NAME=productcatalog
export GITLAB_RECOMMENDATION_PROJECT_NAME=recommendation
export GITLAB_SHIPPING_PROJECT_NAME=shipping
export GITLAB_SHOP_REDIS_PROJECT_NAME=shopredis
## Single repos
export GITLAB_BANK_PROJECT_NAME=bank-of-anthos
export GITLAB_SHOP_PROJECT_NAME=online-boutique
export GITLAB_COCKROACHDB_PROJECT_NAME=cockroachdb
export GITLAB_REDIS_PROJECT_NAME=redis
export GITLAB_SSH_KEY=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm-shared-single-sshkey
# GKE clusters and zones
export GKE1=gke-1-r1a-prod
export GKE2=gke-2-r1b-prod
export GKE3=gke-3-r2a-prod
export GKE4=gke-4-r2b-prod
export GKE5_INGRESS=ingress-config
export GKE1_ZONE=${SUBNET_1_REGION}-a
export GKE2_ZONE=${SUBNET_1_REGION}-b
export GKE3_ZONE=${SUBNET_2_REGION}-a
export GKE4_ZONE=${SUBNET_2_REGION}-b
export GKE5_INGRESS_ZONE=${SUBNET_1_REGION}-a
export KUBECONFIG=${WORKDIR}/asm-kubeconfig
# ASM
export ASM_VERSION=1.10.4-asm.6
export ASM_LABEL=asm-managed
export ASM_CHANNEL=regular
# ACM
export ACM_VERSION=1.6.1
# My IP
export TERMINAL_IP=$(curl -s ifconfig.me)
EOF

source $WORKDIR/vars.sh
gcloud config unset project

echo -e "\n"
title_no_wait "Creating the main folder..."
export MAIN_FOLDER_EXISTS=$(gcloud alpha resource-manager folders list --organization=$ORG_ID | grep ${MAIN_FOLDER_NAME})
if [ ! "$MAIN_FOLDER_EXISTS" ]; then
    gcloud alpha resource-manager folders create \
    --display-name=${MAIN_FOLDER_NAME} \
    --organization=${ORG_ID}
fi

echo -e "\n"
title_no_wait "Getting Main folder ID..."
export MAIN_FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} --filter="displayName=${MAIN_FOLDER_NAME}" --format='value(name)')
[ -z "$MAIN_FOLDER_ID" ] && echo "MAIN_FOLDER_ID is not exported" && exit 1 || echo "Main Folder ID is $MAIN_FOLDER_ID"
echo -e "export MAIN_FOLDER_ID=$MAIN_FOLDER_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating KCC folder..."
export KCC_FOLDER_EXISTS=$(gcloud resource-manager folders list --folder=${MAIN_FOLDER_ID} | grep ${KCC_FOLDER_NAME})
if [ ! "$KCC_FOLDER_EXISTS" ]; then
    gcloud alpha resource-manager folders create \
    --display-name=${KCC_FOLDER_NAME} \
    --folder=${MAIN_FOLDER_ID}
fi

echo -e "\n"
title_no_wait "Getting KCC folder ID..."
export KCC_FOLDER_ID=$(gcloud resource-manager folders list --folder=${MAIN_FOLDER_ID} --filter="displayName=${KCC_FOLDER_NAME}" --format='value(name)')
[ -z "$KCC_FOLDER_ID" ] && echo "KCC_FOLDER_ID is not exported" || echo "KCC Folder ID is $KCC_FOLDER_ID"
echo -e "export KCC_FOLDER_ID=$KCC_FOLDER_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating KCC GCP Project..."
if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
    echo "KCC project already exists."
else
    gcloud projects create ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --folder ${KCC_FOLDER_ID}
    gcloud beta billing projects link ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --billing-account ${BILLING_ACCOUNT}
    gcloud services enable \
    --project ${KCC_PROJECT}-${KCC_FOLDER_ID} \
    container.googleapis.com \
    cloudbilling.googleapis.com \
    cloudbuild.googleapis.com \
    sqladmin.googleapis.com \
    servicenetworking.googleapis.com \
    accesscontextmanager.googleapis.com \
    cloudresourcemanager.googleapis.com
fi

echo -e "\n"
title_no_wait "Creating KCC GKE cluster..."
export KCC_GKE_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} container clusters list)
if [ ! "$KCC_GKE_EXISTS" ]; then
    DTSTART=$(date  --date="yesterday" +"%m-%d-%yT08:00:00Z")
    DTEND=$(date  --date="yesterday" +"%m-%d-%yT12:00:00Z")
    gcloud container clusters create ${KCC_GKE} \
    --project=${KCC_PROJECT}-${KCC_FOLDER_ID} \
    --region=${KCC_GKE_LOCATION} \
    --node-locations=${KCC_GKE_LOCATION}-a,${KCC_GKE_LOCATION}-b \
    --machine-type "e2-standard-4" \
    --num-nodes "3" --min-nodes "3" --max-nodes "3" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --release-channel None \
    --no-enable-autoupgrade \
    --addons ConfigConnector \
    --maintenance-window-start ${DTSTART} \
    --maintenance-window-end ${DTEND} \
    --maintenance-window-recurrence FREQ=DAILY \
    --workload-pool=${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog
fi

touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
echo -e "export KUBECONFIG=$WORKDIR/asm-kubeconfig" >> $WORKDIR/vars.sh
gcloud container clusters get-credentials ${KCC_GKE} --region ${KCC_GKE_LOCATION} --project ${KCC_PROJECT}-${KCC_FOLDER_ID}
kubectl ctx ${KCC_GKE}=gke_${KCC_PROJECT}-${KCC_FOLDER_ID}_${KCC_GKE_LOCATION}_${KCC_GKE}

echo -e "\n"
title_no_wait "Creating KCC GCP SA and assigning IAM roles..."
export KCC_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${KCC_SERVICE_ACCOUNT})
if [ ! "$KCC_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${KCC_SERVICE_ACCOUNT}

    project_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
        roles/accesscontextmanager.policyAdmin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done

    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) += ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > $WORKDIR/kcc-sa-billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} $WORKDIR/kcc-sa-billing-iam-policy.json
fi

echo -e "\n"
title_no_wait "Creating WorkloadIdentity for the KCC GCP SA..."
gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts add-iam-policy-binding \
${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
--member="serviceAccount:${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
--role="roles/iam.workloadIdentityUser"

echo -e "\n"
title_no_wait "Creating ConfigConnector resource in the KCC cluster..."
cat <<EOF > $WORKDIR/${KCC_GKE}-configconnector.yaml
apiVersion: core.cnrm.cloud.google.com/v1beta1
kind: ConfigConnector
metadata:
  # the name is restricted to ensure that there is only one
  # ConfigConnector instance installed in your cluster
  name: configconnector.core.cnrm.cloud.google.com
spec:
  mode: cluster
  googleServiceAccount: "${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com"
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/${KCC_GKE}-configconnector.yaml

echo -e "\n"
title_no_wait "Creating Organization namespace resource in the KCC cluster..."
cat <<EOF > $WORKDIR/${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ${ORG_SHORT_NAME}
  annotations:
    cnrm.cloud.google.com/organization-id: "${ORG_ID}"
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml

echo -e "\n"
title_no_wait "Creating GCP service account for Gitlab CI runner and asigning IAM roles..."
export GITLAB_GCP_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${GITLAB_CI_GCP_SA_NAME})
if [ ! "$GITLAB_GCP_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${GITLAB_CI_GCP_SA_NAME}

    project_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
        roles/accesscontextmanager.policyAdmin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done
fi

echo -e "\n"
title_no_wait "Creating Gitlab GCP service account credentials..."
if [ ! -f "$WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json" ]; then
    gcloud iam service-accounts keys create $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json \
    --iam-account ${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com
    cat $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key.json | base64 > $WORKDIR/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt
fi

echo -e "\n"
title_no_wait "Cloning the repo inside WORKDIR..."
if [ ! -d "$WORKDIR/asm-shared-vpc-single-project-in-workdir" ]; then
    git clone https://gitlab.com/asm7/asm-shared-vpc-single-project.git -b ${INFRA_BRANCH_NAME} $WORKDIR/asm-shared-vpc-single-project-in-workdir
    cd $WORKDIR/asm-shared-vpc-single-project-in-workdir/infrastructure/builder
else
    rm -rf $WORKDIR/asm-shared-vpc-single-project-in-workdir
    git clone https://gitlab.com/asm7/asm-shared-vpc-single-project.git -b ${INFRA_BRANCH_NAME} $WORKDIR/asm-shared-vpc-single-project-in-workdir
    cd $WORKDIR/asm-shared-vpc-single-project-in-workdir/infrastructure/builder
fi

echo -e "\n"
title_no_wait "Creating the builder docker image in GCR..."
export BUILDER_IMAGE_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds list | grep builder)
if [ ! "$BUILDER_IMAGE_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds submit $WORKDIR/asm-shared-vpc-single-project-in-workdir/infrastructure/builder/. --tag=gcr.io/${KCC_PROJECT}-${KCC_FOLDER_ID}/builder
    gsutil defacl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -r -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    cd ${WORKDIR}
fi

echo -e "\n"
title_no_wait "Create a Gitlab SSH key..."
if [ ! -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key ]; then
    mkdir -p ${WORKDIR}/tmp && cd ${WORKDIR}/tmp
    ssh-keygen -t rsa -b 4096 \
    -C "" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key
    cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key | base64 > ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key-base64.txt
    ssh-keygen -t rsa -b 4096 \
    -C "${ADMIN_USER}" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    eval `ssh-agent`
    ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    echo -e "eval \`ssh-agent\` && ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key" >> $WORKDIR/vars.sh
fi

echo -e "\n"
title_no_wait "Adding the Gitlab SSH key..."
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" --form "title=${GITLAB_SSH_KEY}" --form "key=$(cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key.pub)"

export GITLAB_USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user" | jq -r '.id')
[ -z "$GITLAB_USER_ID" ] && echo "GITLAB_USER_ID is not exported" || echo "GITLAB_USER_ID is $GITLAB_USER_ID"
echo -e "export GITLAB_USER_ID=$GITLAB_USER_ID" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Creating the main Gitlab group..."
export GITLAB_MAIN_GROUP_EXISTS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
if [ ! -f "$GITLAB_MAIN_GROUP_EXISTS" ]; then
    curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --form "name=${GITLAB_GROUP_NAME}" --form "path=${GITLAB_GROUP_NAME}" \
    --form "file=@${WORKDIR}/asm-shared-vpc-single-project-in-workdir/admin/scripts/gitlab-parent-group.tar.gz" \
    "https://gitlab.com/api/v4/groups/import"
    # Get group ID
    export GITLAB_GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
    [ -z "$GITLAB_GROUP_ID" ] && echo "GITLAB_GROUP_ID is not exported" || echo "GITLAB_GROUP_ID is $GITLAB_GROUP_ID"
    echo -e "export GITLAB_GROUP_ID=$GITLAB_GROUP_ID" >> $WORKDIR/vars.sh
fi

echo -e "\n"
title_no_wait "Getting the main Gitlab group runner..."
export GITLAB_RUNNER_TOKEN=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID" | jq -r '.runners_token')
[ -z "$GITLAB_RUNNER_TOKEN" ] && echo "GITLAB_RUNNER_TOKEN is not exported" || echo "GITLAB_RUNNER_TOKEN is $GITLAB_RUNNER_TOKEN"
echo -e "export GITLAB_RUNNER_TOKEN=$GITLAB_RUNNER_TOKEN" >> $WORKDIR/vars.sh

echo -e "\n"
title_no_wait "Deploying Gitlab runner helm chart in the kcc cluster..."
helm repo add gitlab https://charts.gitlab.io

cat <<EOF > $WORKDIR/gitlab-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: gitlab
EOF

kubectl --context=${KCC_GKE} apply -f $WORKDIR/gitlab-namespace.yaml

export GITLAB_HELM_EXISTS=$(helm list -n gitlab -ojson | jq -r '.[].name')
if [ ! "$GITLAB_HELM_EXISTS" ]; then
    helm install --namespace gitlab gitlab-runner --set gitlabUrl="https://gitlab.com" \
    --set runnerRegistrationToken="$GITLAB_RUNNER_TOKEN" \
    --set rbac.create=true \
    --set rbac.clusterWideAccess=true gitlab/gitlab-runner
fi

kubectl --context=${KCC_GKE} -n gitlab wait --for=condition=available deployment gitlab-runner-gitlab-runner --timeout=5m

echo -e "\n"
title_no_wait "Getting the shared Gitlab group runner..."
curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'

echo -e "\n"
title_no_wait "Creating Gitlab subgroups and projects for admin, apps and databases..."
export CREATE_REPO_SCRIPT="${WORKDIR}/asm-shared-vpc-single-project-in-workdir/admin/scripts/create_gitlab_repo.sh"
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_INFRA_PROJECT_NAME} --varprefix INFRA
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_CONFIG_PROJECT_NAME} --varprefix CONFIG
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_ADMIN_SUBGROUP_NAME} --repo ${GITLAB_CD_PROJECT_NAME} --varprefix CD
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME} --varprefix BANK_ACCOUNTS_DB
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANK_LEDGER_DB_PROJECT_NAME} --varprefix BANK_LEDGER_DB
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_CONTACTS_PROJECT_NAME} --varprefix CONTACTS
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANKFRONTEND_PROJECT_NAME} --varprefix BANKFRONTEND
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BALANCEREADER_PROJECT_NAME} --varprefix BALANCEREADER
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_LEDGERWRITER_PROJECT_NAME} --varprefix LEDGERWRITER
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_BANKLOADGENERATOR_PROJECT_NAME} --varprefix BANKLOADGENERATOR
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_TRANSACTIONHISTORY_PROJECT_NAME} --varprefix TRANSACTIONHISTORY
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_BANK_SUBGROUP_NAME} --repo ${GITLAB_USERSERVICE_PROJECT_NAME} --varprefix USERSERVICE
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_AD_PROJECT_NAME} --varprefix AD
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CART_PROJECT_NAME} --varprefix CART
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CHECKOUT_PROJECT_NAME} --varprefix CHECKOUT
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_CURRENCY_PROJECT_NAME} --varprefix CURRENCY
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_EMAIL_PROJECT_NAME} --varprefix EMAIL
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOPFRONTEND_PROJECT_NAME} --varprefix SHOPFRONTEND
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOPLOADGENERATOR_PROJECT_NAME} --varprefix SHOPLOADGENERATOR
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_PAYMENT_PROJECT_NAME} --varprefix PAYMENT
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_PRODUCTCATALOG_PROJECT_NAME} --varprefix PRODUCTCATALOG
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_RECOMMENDATION_PROJECT_NAME} --varprefix RECOMMENDATION
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHIPPING_PROJECT_NAME} --varprefix SHIPPING
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_SHOP_SUBGROUP_NAME} --repo ${GITLAB_SHOP_REDIS_PROJECT_NAME} --varprefix SHOP_REDIS
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_DATABASES_SUBGROUP_NAME} --repo ${GITLAB_COCKROACHDB_PROJECT_NAME} --varprefix COCKROACHDB
${CREATE_REPO_SCRIPT} --group ${GITLAB_GROUP_NAME} --subgroup ${GITLAB_DATABASES_SUBGROUP_NAME} --repo ${GITLAB_REDIS_PROJECT_NAME} --varprefix REDIS
source ${WORKDIR}/vars.sh

echo -e "\n"
title_no_wait "Creating CI variables in the main Gitlab group..."
cat ${WORKDIR}/vars.sh | base64 > ${WORKDIR}/vars-base64.txt
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ALL_VARIABLES" --form "value=$(cat ${WORKDIR}/vars-base64.txt)" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${WORKDIR}/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_ACM_KEY" --form "value=$(cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key-base64.txt)" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ADMIN_USER" --form "value=${ADMIN_USER}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GROUP_NAME" --form "value=${GITLAB_GROUP_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_ADMIN_SUBGROUP_NAME" --form "value=${GITLAB_ADMIN_SUBGROUP_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_BANK_SUBGROUP_NAME" --form "value=${GITLAB_BANK_SUBGROUP_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_SHOP_SUBGROUP_NAME" --form "value=${GITLAB_SHOP_SUBGROUP_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=MAIN_FOLDER_ID" --form "value=${MAIN_FOLDER_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=TERMINAL_IP" --form "value=${TERMINAL_IP}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_CHANNEL" --form "value=${ASM_CHANNEL}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_LABEL" --form "value=${ASM_LABEL}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_VERSION" --form "value=${ASM_VERSION}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ACM_VERSION" --form "value=${ACM_VERSION}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=DOUBLE_QUOTES" --form "value=\"" --form "protected=true"

echo -e "\n"
title_no_wait "Verifying Gitlab CI variables..."
curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/groups/"${GITLAB_GROUP_ID}"/variables | jq

echo -e "\n"
title_no_wait "Cloning the infrastructure repo..."
cd ${WORKDIR}
if [ ! -d ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project ]; then
    git clone $GITLAB_INFRA_PROJECT_SSH_URL ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
fi

echo -e "\n"
title_no_wait "Starting the infrastructure repo pipeline..."
cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/infrastructure/. ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
cd ${WORKDIR}/${ORG_SHORT_NAME}-asm-shared-vpc-single-project
git add .
git commit -m "initial commit"
git push

echo -e "\n"
title_no_wait "Inspect the pipeline by clicking on the following link..."
echo -n "${GITLAB_INFRA_PROJECT_WEB_URL}/-/pipelines"
echo -e "\n"

export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/pipelines" | jq -r '.[0].status')
export INFRA_TRIES=0
until [[ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]] || [[ $INFRA_GITLAB_PIPELINE_STATUS = "success" ]]; do
    echo -e "\e[93mWaiting for infrastructure pipeline to get going..."
    sleep 5
    export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/pipelines" | jq -r '.[0].status')
    export INFRA_TRIES=$((INFRA_TRIES+1))
    if [ $INFRA_TRIES = 50 ]; then 
        echo -e "\e[91mInfrastructure pipeline is not starting. Exiting...\n"
        exit 1
    fi
done

if [ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]; then echo -e "\e[92mInfrastructure pipeline is running...\n"; fi
if [ $INFRA_GITLAB_PIPELINE_STATUS = "success" ]; then echo -e "\e[92mInfrastructure pipeline has finished successfully.\n"; fi


while [ $INFRA_GITLAB_PIPELINE_STATUS = "running" ]; do
    echo -e "Successful jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=success" | jq '.[].name'
    echo -e "Running jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=running" | jq '.[].name'
    echo -e "Pending jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=pending" | jq '.[].name'
    echo -e "Upcoming jobs: "
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/jobs?scope[]=created" | jq '.[].name'
    echo -e "Gitlab pipeline is $INFRA_GITLAB_PIPELINE_STATUS, sleeping for 10 seconds..."
    sleep 10
    export INFRA_GITLAB_PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_INFRA_PROJECT_ID}/pipelines" | jq -r '.[].status')
done

if [ $INFRA_GITLAB_PIPELINE_STATUS = "failed" ]; then 
    echo -e "\e[92mInfrastructure pipeline has failed!\n"
    echo -e "\e[92mStopping automatic build procedure.\n"
    exit 1
fi

echo -e "\n"
title_no_wait "Getting cluster credentials to all clusters..."
export INFRA_FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
[ -z "$INFRA_FOLDER_ID" ] && echo "INFRA_FOLDER_ID is not exported" || echo "INFRA_FOLDER_ID is $INFRA_FOLDER_ID"
echo -e "export INFRA_FOLDER_ID=$INFRA_FOLDER_ID" >> $WORKDIR/vars.sh
echo -e "export FOLDER_ID=$INFRA_FOLDER_ID" >> $WORKDIR/vars.sh
gcloud config set project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=INFRA_FOLDER_ID" --form "value=${INFRA_FOLDER_ID}" --form "protected=true"
curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=FOLDER_ID" --form "value=${INFRA_FOLDER_ID}" --form "protected=true"

gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}
gcloud container clusters get-credentials ${GKE5_INGRESS} --zone ${GKE5_INGRESS_ZONE} --project ${SVC_1_PROJECT}-${INFRA_FOLDER_ID}

kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE1_ZONE}_${GKE1}
kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE2_ZONE}_${GKE2}
kubectl ctx ${GKE3}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE3_ZONE}_${GKE3}
kubectl ctx ${GKE4}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE4_ZONE}_${GKE4}
kubectl ctx ${GKE5_INGRESS}=gke_${SVC_1_PROJECT}-${INFRA_FOLDER_ID}_${GKE5_INGRESS_ZONE}_${GKE5_INGRESS}

############ ADMIN REPOS ##################

# mkdir -p ${WORKDIR}/repos
# cd ${WORKDIR}/repos
# # bootstrap shared-cd repo
# [ ! -d ${WORKDIR}/repos/shared-cd ] && git clone ${GITLAB_CD_PROJECT_SSH_URL} ${WORKDIR}/repos/shared-cd
# cd ${WORKDIR}/repos/shared-cd && git pull
# cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/shared-cd/. ${WORKDIR}/repos/shared-cd/.
# cd ${WORKDIR}/repos/shared-cd
# git add .
# git commit -m "initial commit"
# git push || true

# # Create git-creds-secrets for individual namespaces
# kubectl create secret generic git-creds \
# --from-file=ssh=${WORKDIR}/tmp/${GITLAB_SSH_KEY}-acm-key --dry-run=client -oyaml > ${WORKDIR}/tmp/secret-git-creds.yaml

# # bootstrap config repo
# [ ! -d ${WORKDIR}/repos/config ] && git clone ${GITLAB_CONFIG_PROJECT_SSH_URL} ${WORKDIR}/repos/config
# cd ${WORKDIR}/repos/config && git pull
# rm -rf ${WORKDIR}/repos/config/namespaces
# cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/config/. ${WORKDIR}/repos/config/.
# for app in bank-of-anthos online-boutique cockroachdb redis
# do
#     cd ${WORKDIR}/repos/config/namespaces/${app}
#     for FOLDER in $(ls -d * | xargs -n 1 echo)
#     do
#         export FOLDER=$FOLDER
#         for f in $(find ${WORKDIR}/repos/config/namespaces/${app}/${FOLDER} -regex '.*namespace\.yaml_tmpl'); do envsubst < $f > "${WORKDIR}/repos/config/namespaces/${app}/${FOLDER}/$(basename ${f%.yaml_tmpl}.yaml)"; done
#         rm -rf ${WORKDIR}/repos/config/namespaces/${app}/${FOLDER}/*.yaml_tmpl
#     done
# done
# cd ${WORKDIR}/repos/config
# git pull
# git add .
# git commit -m "initial commit"
# git push || true

# gsutil cp gs://config-management-release/released/1.6.1/linux_amd64/nomos ${WORKDIR}/tmp/nomos
# chmod +x ${WORKDIR}/tmp/nomos
# export NOMOS_161=${WORKDIR}/tmp/nomos
# echo -e "export NOMOS_161=${WORKDIR}/tmp/nomos" >> $WORKDIR/vars.sh
# # Verify nomos status
# ${NOMOS_161} status

# # CockroachDB
# export repo=cockroachdb
# [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_DATABASES_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
# cd ${WORKDIR}/repos/${repo} && git pull
# cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/databases/${repo}/. ${WORKDIR}/repos/${repo}/.
# cd ${WORKDIR}/repos/${repo}
# envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
# rm -rf .gitlab-ci.yml_tmpl
# git add .
# git commit -m "initial commit"
# git push || true

# echo -e "\e[95mSleeping for 60 seconds in between CockroachDB and RedisDB pipelines...\e[0m"
# sleep 60

# # RedisDB
# export repo=redis
# [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_DATABASES_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
# cd ${WORKDIR}/repos/${repo} && git pull
# cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/databases/${repo}/. ${WORKDIR}/repos/${repo}/.
# cd ${WORKDIR}/repos/${repo}
# envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
# rm -rf .gitlab-ci.yml_tmpl
# git add .
# git commit -m "initial commit"
# git push || true

# # Bank of Anthos application repo
# bank_repos=(
#     $GITLAB_BANK_ACCOUNTS_DB_PROJECT_NAME
#     $GITLAB_BANK_LEDGER_DB_PROJECT_NAME
#     $GITLAB_CONTACTS_PROJECT_NAME
#     $GITLAB_BANKFRONTEND_PROJECT_NAME
#     $GITLAB_BALANCEREADER_PROJECT_NAME
#     $GITLAB_LEDGERWRITER_PROJECT_NAME
#     $GITLAB_BANKLOADGENERATOR_PROJECT_NAME
#     $GITLAB_TRANSACTIONHISTORY_PROJECT_NAME
#     $GITLAB_USERSERVICE_PROJECT_NAME
# )
# for repo in "${bank_repos[@]}"
# do
#     echo -e "\e[95mBuilding $repo repo...\e[0m"
#     [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_BANK_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
#     cd ${WORKDIR}/repos/${repo} && git pull
#     cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/bank-of-anthos/${repo}/. ${WORKDIR}/repos/${repo}/.
#     cd ${WORKDIR}/repos/${repo}
#     envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
#     rm -rf .gitlab-ci.yml_tmpl
#     git add .
#     git commit -m "initial commit"
#     git push || true
#     echo -e "\e[95mSleeping for 15 seconds in between builds to avoid hitting Cloud Build API rate limit...\e[0m"
#     sleep 15
# done

#     echo -e "\e[95mSleeping for 60 seconds in between Bank of Anthos and Online Boutique Application pipelines to avoid hitting Cloud Build API rate limit...\e[0m"
#     sleep 60

# # Online Boutique application repo
# shop_repos=(
#     $GITLAB_AD_PROJECT_NAME
#     $GITLAB_CART_PROJECT_NAME
#     $GITLAB_CHECKOUT_PROJECT_NAME
#     $GITLAB_CURRENCY_PROJECT_NAME
#     $GITLAB_EMAIL_PROJECT_NAME
#     $GITLAB_SHOPFRONTEND_PROJECT_NAME
#     $GITLAB_SHOPLOADGENERATOR_PROJECT_NAME
#     $GITLAB_PAYMENT_PROJECT_NAME
#     $GITLAB_PRODUCTCATALOG_PROJECT_NAME
#     $GITLAB_RECOMMENDATION_PROJECT_NAME
#     $GITLAB_SHIPPING_PROJECT_NAME
#     $GITLAB_SHOP_REDIS_PROJECT_NAME
# )
# for repo in "${shop_repos[@]}"
# do
#     echo -e "\e[95mBuilding $repo repo...\e[0m"
#     [ ! -d ${WORKDIR}/repos/${repo} ] && git clone git@gitlab.com:${GITLAB_GROUP_NAME}/${GITLAB_SHOP_SUBGROUP_NAME}/${repo}.git ${WORKDIR}/repos/${repo}
#     cd ${WORKDIR}/repos/${repo} && git pull
#     cp -r ${WORKDIR}/asm-shared-vpc-single-project-in-workdir/online-boutique/${repo}/. ${WORKDIR}/repos/${repo}/.
#     cd ${WORKDIR}/repos/${repo}
#     envsubst < .gitlab-ci.yml_tmpl > .gitlab-ci.yml
#     rm -rf .gitlab-ci.yml_tmpl
#     git add .
#     git commit -m "initial commit"
#     git push || true
#     echo -e "\e[95mSleeping for 15 seconds in between builds to avoid hitting Cloud Build API rate limit...\e[0m"
#     sleep 15
# done

# # Set up your terminal environment
# ${SCRIPT_DIR}/get_all_gitlab_pipeline_status.sh

# echo -e "\e[95mCopy and paste the following commands in your terminal:\e[0m"
# echo -e "source ${WORKDIR}/vars.sh\n"

# echo -e "\e[95mYou can check the status of your application pipelines by running the following command:\e[0m"
# echo -e "${SCRIPT_DIR}/get_all_gitlab_pipeline_status.sh\n"

# echo -e "\e[95mYou can check the status of your Google managed certs by running the following command:\e[0m"
# echo -e "gcloud --project ${SVC_1_PROJECT}-${FOLDER_ID} compute ssl-certificates list\n"

# echo -e "\e[95mYou can access the Bank of Anthos and Online Boutique applications at the following links once all pipelines are successfully finished:\e[0m"
# echo -e "https://shop.endpoints.${SVC_1_PROJECT}-${INFRA_FOLDER_ID}.cloud.goog"
# echo -e "https://bank.endpoints.${SVC_1_PROJECT}-${INFRA_FOLDER_ID}.cloud.goog"



